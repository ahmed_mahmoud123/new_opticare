import { Injectable } from '@angular/core';
declare var $:any;
@Injectable()
export class PrintService {

  constructor() { }

  static print(sectionId,orientation='landscape'):void{
    let printContents, popupWin;
    $("#"+sectionId).find('input[type=text]').attr('value', function (i, val) { return val; });
    $("#"+sectionId).find('input[type=checkbox],input[type=radio]').attr('checked', function () { return this.checked; });
    $("#"+sectionId).find('textarea').html(function () { return this.value; });
    $("#"+sectionId).find('select').find(':selected').attr('selected', 'selected');
    printContents = $("#"+sectionId).html();
    popupWin = window.open('', '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
          <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <style type="text/css" media="print">
              @page {
                size: A4 ${orientation};
                max-height:100%;
                max-width:100%
              }
              *{
                caption-side: top !important;
                font-family: "Open Sans", sans-serif;
              }
              img{
                background-color:#337ab7 !important;
                width:90%;
                margin:10px !important;
              }
              body {
                width:100%;
                height:100%;

               }
               .col-md-4{
                 border-right : 1px dotted #000;
                 border-left : 1px dotted #000;
               }
               *{
                 font-size:9px !important;
               }
               .box{
                 border:1px solid #000;
                 min-height:90px;
                 margin:20px;
                 width:100%;
               }
              </style>

            <script>
              window.onload = function(){
                  window.print();
                  window.close();
              }
            </script>
        </head>
        <body onload="">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
