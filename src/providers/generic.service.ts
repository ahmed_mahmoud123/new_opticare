import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class GenericService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://35.190.155.16:4000";
    //this.api_url="http://localhost:4000";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  prepareRequestHeader(){
    this.headers = new Headers ({'Content-Type': 'application/json' });
    //console.log(localStorage.getItem('api_token'));
    return {headers:this.headers};
  }
  post(urn,object){
    return this.http.post(`${this.api_url}/${urn}`,JSON.stringify(object),this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  get(urn){
    return this.http.get(`${this.api_url}/${urn}`,this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  getPerPage(urn,searchObject,page){
    return this.http.post(`${this.api_url}/${urn}`,JSON.stringify({
      query:searchObject,
      page:page
    }),this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  delete(urn,ids){
    ids=ids.map(row=>{return row._id;});
    ids=JSON.stringify(ids);
    return this.http.get(`${this.api_url}/${urn}/${ids}`,this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }
}


// User Component
                  // User Service
                                // HTTP Service
