export class HelperMethods{

  public static addProperties(obj:any,newProperties:any){
    for(let property in newProperties){
      obj[property] = newProperties[property];
    }
    return obj;
  }

  public static cloneArray(arr){
    let newArray = [];
    for (let i = 0; i < arr.length; i++) {
        newArray[i]=arr[i];
    }
    return newArray;
  }
}
