export class TableFactory{

  public static generateTable(col,headers:Array<string>=[],rows:Array<any>=[],name:string=undefined){
    return {
      type:"table",
      col:col,
      headers:headers,
      rows:rows,
      name:name
    }
  }



}
