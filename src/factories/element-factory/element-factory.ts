export class ElementFactory{

  public static generateInput(name:string,label:string,col:number,tabIndex:number = -1,value:string="",required:boolean=false,validators:Array<string>=[],classes:Array<string>=[],width:string=undefined){
    return {
      name:name,
      label:label,
      type:"input",
      col:col,
      classes:classes,
      //tabIndex:tabIndex,
      validators:validators,
      required:required,
      value:value,
      readOnly:false,
      width:width
    }
  }

  public static generateLabel(label:string,col:number,value:string=""){
    return {
      label:label,
      type:"label",
      col:col,
      value:value
    }
  }

  public static generateSelect(name:string,label:string,col:number,options:Array<any>=undefined,dataSource:string=undefined,value:string="",popupUrl:string=undefined,required:boolean=false,validators:Array<string>=[],classes:Array<string>=[]){
    return {
      name:name,
      label:label,
      type:"select",
      col:col,
      classes:classes,
      validators:validators,
      required:required,
      value:value,
      options:options,
      dataSource:dataSource,
      popupUrl:popupUrl
    }
  }

  public static generateRadio(name:string,label:string,col:number,options:Array<any>=undefined,dataSource:string=undefined,value:string="",required:boolean=false,validators:Array<string>=[],classes:Array<string>=[]){
    return {
      name:name,
      label:label,
      type:"radio",
      col:col,
      classes:classes,
      validators:validators,
      required:required,
      value:value,
      options:options,
      dataSource:dataSource
    }
  }

  public static generateCustomInput(type:string,name:string,label:string,col:number,tabIndex:number=-1,value:string="",required:boolean=false,validators:Array<string>=[],classes:Array<string>=[]){
    let input = {
      name:name,
      label:label,
      col:col,
      type:type,
      classes:classes,
      //tabIndex:tabIndex,
      validators:validators,
      required:required,
      value:value
    }
    return input;
  }

  public static generateCheckbox(name:string,label:string,col:number,options:Array<any>=undefined){
    return {
      name:name,
      label:label,
      type:"checkbox",
      col:col,
      options:options
    }
  }

  public static generateRange(name:string,label:string,col:number){
    return {
      name:name,
      label:label,
      type:"range",
      col:col
    }
  }

  public static generateButton(buttons:Array<any>=[]){
    let btns = [];
    buttons.forEach(btn=>{
      btns.push({
        text:btn.text,
        id:btn.id,
        classes:btn.classes?btn.classes : 'btn blue',
        icon:btn.icon,
        custom : btn.custom,
        fn:btn.fn
      })
    });
    return {
      type:"button",
      buttons:btns
    }
  }

  public static generateReExaminationSelect(){
    let months=[{text:"1 Month",value:1}];
    for (let i = 2; i <= 12; i++) {
        months.push({text:`${i} Months`,value:i});
    }
    months.push({text:"18 Months",value:18});
    months.push({text:"24 Months",value:24});
    return ElementFactory.generateSelect("rexam_after","Re-examination After",12,months,undefined,"12");
  }
}
