import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormComponent } from './dynamic-form.component';
import { SpinnerModule } from 'angular2-spinner/dist';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NouisliderModule } from 'ng2-nouislider';
import { ToastrModule } from 'ngx-toastr';

import { DynamicField } from './components/dynamic.derictive';

import { SelectComponent } from './components/select/select.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { EmailComponent } from './components/email/email.component';
import { PhoneComponent } from './components/phone/phone.component';
import { RadioComponent } from './components/radio/radio.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { TextFieldComponent } from './components/textfield/textfield.component';
import { DOBComponent } from './components/dob/dob.component';
import { ButtonComponent } from './components/button/button.component';
import { PopupComponent } from './components/popup/popup.component';
import { PopupButtonComponent } from './components/popup-button/popup-button.component';
import { TableComponent } from './components/table/table.component';
import { LabelComponent } from './components/label/label.component';
import { HorizontalComponent } from './components/horizontal/horizontal.component';
import { TagComponent } from './components/tag/tag.component';

import { ValidationComponent } from './components/validation/validation.component';

import { KeysPipe } from '../../pipes/keys.pipe';

import { RangeComponent } from './components/range/range.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SpinnerModule,
    TagInputModule,
    BrowserAnimationsModule,
    NouisliderModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
    }),
  ],
  declarations: [
    DynamicFormComponent,
    DynamicField,
    TextFieldComponent,
    SelectComponent,
    TextareaComponent,
    EmailComponent,
    PhoneComponent,
    RadioComponent,
    LabelComponent,
    CheckboxComponent,
    KeysPipe,
    DOBComponent,
    ButtonComponent,
    ValidationComponent,
    PopupComponent,
    PopupButtonComponent,
    TableComponent,
    HorizontalComponent,
    TagComponent,
    RangeComponent
  ],
  exports: [
    DynamicFormComponent,
    PopupComponent,

  ],
  entryComponents:[
    TextFieldComponent,
    SelectComponent,
    TextareaComponent,
    EmailComponent,
    PhoneComponent,
    RadioComponent,
    CheckboxComponent,
    DOBComponent,
    ButtonComponent,
    ValidationComponent,
    PopupComponent,
    PopupButtonComponent,
    TableComponent,
    LabelComponent,
    HorizontalComponent,
    TagComponent,
    RangeComponent
  ]
})

export class DynamicFormModule { }
