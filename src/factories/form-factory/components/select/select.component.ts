import { Component, OnInit } from '@angular/core';
import { GenericService } from '../../../../providers/generic.service';
declare var $:any;
@Component({
  templateUrl : './select.component.html'
})

export class SelectComponent implements  OnInit {
  config:any;
  group:any;
  popupConfig:any;
  private fromPopup:boolean = false;
  constructor(private __genericService:GenericService){

  }

  ngOnInit(){

    if(this.config.popupUrl)
      this.buildPopupConfig();

    if(this.config.dataSource){
      this.getData(this.config.dataSource);
    }else{
      this.config.options.map(option=>{
        if(option.text)
        option.text  = option.text.toUpperCase();
        return option;
      });
    }

    /*this.group.valueChanges.subscribe(val => {
      let placeholder = this.config.label;
      $('#'+this.config.name).select2({
        width:"100%",
        placeholder:"Select "+placeholder
      });
    });*/
  }


  ngAfterViewInit(){
    let self = this;
    $('#'+this.config.name).change(function(e){
      self.group.controls[$(this).attr('id')].patchValue($(this).val());
    });

    let placeholder = this.config.label;
    setTimeout(()=>{
      $('#'+this.config.name).select2({
        width:"100%",
        placeholder:"Select "+placeholder
      })
    },200);
  }



  getData(url:string){
    this.__genericService.get(url).then(data=>{
      if(data.length){
        this.config.options = data.map(item => { return {value:item._id,text:item.name?item.name.toUpperCase():'' } });
        if(this.fromPopup){
          this.group.controls[this.config.name].setValue(this.config.options[this.config.options.length-1].value);
          this.fromPopup = false;
        }
      }
    });
  }

  afterPopupsubmit(event){
    this.fromPopup = true;
    this.getData(this.config.dataSource);
    setTimeout(()=>{
      $('#modal_'+this.config.name).modal('hide');
    },500)
  }
  buildPopupConfig(){
    this.popupConfig={
      "props":{id:"popup",action:this.config.popupUrl,msg:"Done"},
      "accordions":[{
        "title":"Add "+this.config.label,
        "rows":
        [
          [
            {
              name:"name",
              label:this.config.label,
              type:"input",
              validators:["required"],
              required:true,
              col:12,
              classes:[],
            }
          ],
          [
            {
              type:"popButton",
              buttons:[
                {
                  text:"save",
                  id:"save",
                  classes:'btn blue'
                }
              ]

            }
          ]
        ]
      }]
    }
  }
}
