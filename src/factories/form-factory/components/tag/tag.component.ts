import { Component } from '@angular/core';
declare var $:any;
@Component({
  templateUrl : './tag.component.html'
})

export class TagComponent{
  config:any;
  group:any;
  //13
  keys:Array<number>=[188];

  submit(evt){
    if(evt.which == 13){
        $("#search").trigger("click");
    }
  }

  tagRemoved(evt){
    if(this.config.name == "order_tags"){
      $("#search").trigger("click");
    }
  }
}
