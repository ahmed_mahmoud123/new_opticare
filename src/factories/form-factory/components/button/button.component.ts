import { Component } from '@angular/core';

@Component({
  templateUrl : './button.component.html'
})

export class ButtonComponent{
  config:any;
  group:any;

  click(evt,btn){
    if( btn.id == "delete" || btn.custom || btn.id == "save_order"){
      evt.preventDefault();
      if(btn.fn && btn.self) btn.fn.bind(btn.self)();
      else if (btn.fn) {
        btn.fn();
      }

    }

    this.group.controls['source'].setValue(btn.id);
  }


}
