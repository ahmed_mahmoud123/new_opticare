import { Component,Input } from '@angular/core';
import { ValidatorMessages } from '../../../../validators/validator-messages';
@Component({
  selector:'validation',
  templateUrl : './validation.component.html',
  styleUrls:['./validation.component.css']
})

export class ValidationComponent{
  validatorMessages:any = ValidatorMessages.messages;
  @Input() validators:Array<any>;
  @Input() name:any;
  @Input() label:any;
  @Input() group:any;
}
