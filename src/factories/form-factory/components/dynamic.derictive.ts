import { ComponentFactoryResolver,OnInit,Input,Directive,ViewContainerRef } from '@angular/core';

import { ExamHistoryComponent } from '../../../modules/exam/history/history.component';
import { CartComponent } from '../../../modules/inventory/cart/cart.component';

import { SelectComponent } from './select/select.component';
import { TextareaComponent } from './textarea/textarea.component';
import { EmailComponent } from './email/email.component';
import { PhoneComponent } from './phone/phone.component';
import { RadioComponent } from './radio/radio.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { TextFieldComponent } from './textfield/textfield.component';
import { DOBComponent } from './dob/dob.component';
import { ButtonComponent } from './button/button.component';
import { PopupButtonComponent } from './popup-button/popup-button.component';
import { TableComponent } from './table/table.component';
import { LabelComponent } from './label/label.component';
import { HorizontalComponent } from './horizontal/horizontal.component';
import { TagComponent } from './tag/tag.component';
import { RangeComponent } from './range/range.component';

import { PaymentSummaryComponent } from '../../../modules/order/payment-summary/payment-summary.component';

const components={
  'input' : TextFieldComponent,
  'select':SelectComponent,
  'radio' : RadioComponent,
  'checkbox':CheckboxComponent,
  'email' : EmailComponent,
  'textarea':TextareaComponent,
  'phone':PhoneComponent,
  'dob':DOBComponent,
  'button':ButtonComponent,
  'popButton':PopupButtonComponent,
  'table':TableComponent,
  'label' :LabelComponent,
  'horizontal':HorizontalComponent,
  'tag':TagComponent,
  'exam-history' : ExamHistoryComponent,
  'order-cart' : CartComponent,
  'payment-summary':PaymentSummaryComponent,
  'range':RangeComponent
}

@Directive({
  selector : "[dynamicField]"
})


export class DynamicField implements OnInit{
  @Input() config;
  @Input() group;
  component;
  constructor(private resolver:ComponentFactoryResolver,private container:ViewContainerRef){

  }

  ngOnInit(){
    const component = components[this.config.type];
    const factory = this.resolver.resolveComponentFactory<any>(component);
    this.component = this.container.createComponent(factory);
    this.component.instance.config = this.config;
    this.component.instance.group = this.group ;
  }
}
