import { Component , Input , Output ,EventEmitter} from '@angular/core';
declare var $:any ;

@Component({
  selector:'popup',
  templateUrl : './popup.component.html',
  styleUrls:['./popup.component.css']
})

export class PopupComponent{
  @Input() id:any;
  @Input() config :any;
  @Input() defaultSubmit : boolean = true;
  @Output() submited: any = new EventEmitter();
  @Output() popupAfterSubmit = new EventEmitter();
  @Output() formChange = new EventEmitter();
  @Output() groupCreated = new EventEmitter();

  constructor(){

  }

  onSubmit($event){
    $("#modal_"+this.id).modal('hide');
    this.submited.emit($event)
  }

  submit($event){
    this.popupAfterSubmit.emit($event);
  }
}
