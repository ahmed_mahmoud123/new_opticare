import { Component } from '@angular/core';

@Component({
  templateUrl : './range.component.html',
})

export class RangeComponent{
  config:any;
  group:any;
  rangeConfig = {
    start: [300, 1000],
    tooltips: true,
    connect: true,
	  range: {
		     'min': [ 0 ],
		       'max': [ 1000 ]
  	}
  }
}
