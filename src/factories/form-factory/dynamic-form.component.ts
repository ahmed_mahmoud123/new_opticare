import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { FormGroup , Validators ,FormBuilder ,ValidatorFn, FormControl , FormArray } from '@angular/forms';
import { GenericService } from '../../providers/generic.service';
import { EmailValidator } from '../../validators/emailValidator';
import { CustomValidator } from '../../validators/customValidator';
import { ToastrService } from 'ngx-toastr';

import { ActivatedRoute,Router } from '@angular/router';
declare var $:any;
const validations = {
  'required' : Validators.required,
  'email' : EmailValidator.isValid,
  'not_empty':CustomValidator.isEmpty,
  "table_required":CustomValidator.isTableRequired,
  "valid_date":CustomValidator.isDate
}

@Component({
  selector : "generic-form",
  templateUrl:"./generic-form.html",
  styleUrls:['./generic-form.css'],
})

export class DynamicFormComponent implements OnInit {
  group:any;
  @Input() config : any = [];
  @Input() defaultSubmit : boolean = true;
  @Output() afterSubmited : any = new EventEmitter();
  @Output() afterGroupPopulate : any = new EventEmitter();
  @Output() submited: any = new EventEmitter();
  @Output() formChange: any = new EventEmitter();
  @Output() groupCreated: any = new EventEmitter();
  @Output() modelData: any = new EventEmitter();
  private clearObject = {};
  working:boolean = false ;
  error:boolean = false ;
  success:boolean = false ;
  source:string;
  private tabIndex:number;
  private config_id;
  private byIdUrl;
  constructor(private toastrService:ToastrService,private router:Router,private route:ActivatedRoute,private __genericService:GenericService,private fb:FormBuilder){

  }

  ngOnInit(){
    this.createGroup();
    this.byIdUrl = this.config.props.byIdUrl;
    this.config_id = this.config.props.id;
    if(this.config.props.edit == true){
      this.populateModel();
    }
  }

  ngAfterViewInit(){
    let self=this;
    $('.select2').change(function(e){
      $(this).focus();
      if(self.group.controls[e.target.id]){
        self.group.controls[e.target.id].patchValue(e.target.value);
        self.formChange.emit({group:self.group,event:e});
      }
    });
    $('select').on('select2:close',(function(){$(this).focus();}));
  }

  ngOnChanges(change) {
    if(change.config && change.config.currentValue && !change.defaultSubmit){
      this.createGroup();
    }
    this.setSelect2Values();
  }

  setSelect2Values(){
    setTimeout(()=>{
      $('.select2').trigger('change.select2');
    },100);
  }

  ngDoCheck(){
    if (this.byIdUrl != this.config.props.byIdUrl && this.config.props.edit){
      this.populateModel();
      this.byIdUrl = this.config.props.byIdUrl;
      setTimeout(()=>{
        //$('.select2').trigger('change.select2');
      },500);
    }
  }

  createGroup():void {
    this.tabIndex = 1;
    this.group = this.fb.group({});
    this.group.addControl("source",this.fb.control(""));
    this.config["accordions"].forEach(accordion=>{
      accordion["rows"].forEach(row=>{
        row.forEach(element=>{

          if(element.type == "checkbox"){
            let formArray = [];
            element.options.forEach(option=>{
              formArray.push(new FormControl(option.selected));
            });
            this.group.addControl(element.name,this.fb.array(formArray));
          }else if(element.type == "table"){
            this.buildTabelControls(element,this.group);
          }else if(element.type =="horizontal"){
            this.buildHorizontalControls(element,this.group);
          }else{
            if(element.type == "input" && !element.readOnly)
              this.clearObject[element.name] = "";
              this.setTabIndex(element);
            if(element.name)
              this.group.addControl(element.name,this.fb.control(element.value,Validators.compose(this.buildValidatorsArray(element.validators))));
          }
        });
      });
    });
    this.groupCreated.emit(this.group);
  }

  setTabIndex(element){
    if((element.type == "select" || element.type == "input" || element.type == "textarea" ||element.type == "phone" ||element.type == "email" || element.type == "dob") && !element.readOnly){
      element.tabIndex = this.tabIndex++;
    }
  }
  submit(model:any){
    console.log(model)
    if(this.config.props.updated_id)
      model._id = this.config.props.updated_id;
    if(this.defaultSubmit){
      window.scroll(0,0);
      this.working = true;
      this.__genericService.post(this.config.props.action,model).then(result=>{
        this.onSuccess(result);
      },err=>{
        this.onError();
      }).catch(err=>{
        this.onError();
      });
    }else{
      let newModel={};
      for(let propery in model){
        if(model[propery]!="" && model[propery]!= null)
          newModel[propery] = model[propery]
      }
      this.submited.emit(newModel);
    }
  }

  onError(){
    this.hideMsg();
    this.success = false;
    this.error = true;
    this.working = false;
  }

  onSuccess(result=undefined){
    this.toastrService.success(this.config.props.msg,'Success');
    this.hideMsg();
    if(result._id)
      this.group.value._id = result._id;
    //this.afterSubmited.emit(this.group.value);
    result.source = this.group.value.source;
    this.afterSubmited.emit(result);
    if(!this.config.props.edit && !this.config.props.preventClear)
      this.clear();
    this.success = true;
    this.error = false;
    this.working = false;
    if(this.config.props.redirectUrl && !this.config.props.edit){
      this.router.navigate([this.config.props.redirectUrl]);
    }
  }

  buildValidatorsArray(rules):Array<ValidatorFn>{
    let validators:Array<ValidatorFn> = [];
    if(rules){
      rules.forEach(rule=>{
        validators.push(validations[rule]);
      });
    }
    return validators;
  }

  populateModel(){
    let url = this.config.props.byIdUrl;
    if(!this.config.props.fullUrl){
      url +=this.route.snapshot.params['id'];
    }
    this.working = true;
    this.__genericService.get(url).then(model=>{
      console.log(model);
      this.working = false;
      if(model){
        this.modelData.emit(model);
        for(let property in model){
          if(typeof model[property] != "object")
            model[property] = model[property].toString().toLowerCase();
        }
        for(let property in this.group.controls){
          if(property != "examData")
            this.group.controls[property].patchValue(model[property]?model[property]:'-');
        }
        this.setSelect2Values();
        this.afterGroupPopulate.emit(model);
      }
    }).catch(err=>this.working = false);
  }
  buildTabelControls(element,group){
    element.rows.forEach(row=>{
      row.forEach(field=>{
        if(typeof(field) == "object"){
          this.setTabIndex(field);
          group.addControl(field.name,this.fb.control(field.value,Validators.compose(this.buildValidatorsArray(field.validators))));
        }
      })
    });
  }

  buildHorizontalControls(element,group){
    element.rows.forEach(row=>{
        if(typeof(row) == "object"){
          this.setTabIndex(row);
          group.addControl(row.name,this.fb.control(row.value,Validators.compose(this.buildValidatorsArray(row.validators))));
        }
    });
  }

  clear(){
    for(let property in this.clearObject){
      if(this.group.controls[property])
        this.group.controls[property].patchValue("");
    }
  }

  hideMsg(){
    setTimeout((self)=>{
      self.success = false;
      self.error = false;
    },2000,this);
  }
}
