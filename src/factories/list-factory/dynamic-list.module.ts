import { NgModule } from '@angular/core';
import { DynamicListComponent } from './dynamic-list.component';
import { CommonModule } from '@angular/common';

import { SpinnerModule } from 'angular2-spinner/dist';


@NgModule({
  imports: [
    CommonModule,
    SpinnerModule
  ],
  declarations: [
    DynamicListComponent
  ],
  exports: [
    DynamicListComponent
  ],
  entryComponents:[

  ]
})

export class DynamicListModule { }
