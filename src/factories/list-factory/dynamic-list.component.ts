import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { GenericService } from '../../providers/generic.service';
declare var $:any;
@Component({
  selector : "generic-list",
  templateUrl:"./generic-list.component.html"
})

export class DynamicListComponent implements OnInit {
  working:boolean = false;
  data:Array<any>=[];
  total:number=0;
  page=1;
  last_page=0;
  checkAll:boolean=false;
  @Output() rowClicked: any = new EventEmitter();
  @Input() config: any;
  @Input() set searchObject(value: any) {
    this.config.searchObject = value;
    this.page = 1;
    this.getData();
  }
  constructor(private __genericService:GenericService){

  }

  ngOnInit(){
    this.getData();
  }

  getData(page=1){
    this.working=true;
    this.__genericService.getPerPage(this.config.dataSource,this.config.searchObject,page).then(data=>{
      this.working=false;
      if(data.docs){
        console.log(data.docs)
        this.data=data.docs.map(doc=>{
          Object.keys(doc).forEach(key=>{
            if(typeof(doc[key]) == "string"){
              doc[key] = doc[key].toUpperCase();
            }
          });
          if(this.config.mappingFn){
            doc = this.config.mappingFn(doc);
          }
          return doc;
        });
        this.total=data.total;
        this.last_page=Math.ceil(this.total/10);
      }
      this.convertToDataTable();
    })
  }

  first(event){
    event.preventDefault();
    this.page=1;
    this.getData();
  }

  last(event){
      event.preventDefault();
      this.getData(this.last_page);
  }

  next(){
      event.preventDefault();
      this.getData(++this.page);
  }

  prev(){
      event.preventDefault();
      this.getData(--this.page);
  }

  onChange(value,checked){
    this.checkAll=true;
    for (let i = 0; i < this.data.length; i++) {
        if(value==this.data[i]._id){
          this.data[i].checked=checked;
        }
        if(!this.data[i].checked)
          this.checkAll=false;
    }
  }

  toggleAll(checked){
    this.checkAll=checked;
    for (let i = 0; i < this.data.length; i++) {
          this.data[i].checked=checked;
    }
  }

  delete(){
    if(confirm('are you sure ?')){
      let deleted_rows=this.data.filter(row=>{
        return row.checked==true;
      });
      this.__genericService.delete(this.config.deleteUrl,deleted_rows).then(patients=>{
        this.getData();
      })
    }
  }

  clicked(e,row_id){
    e.preventDefault();
    this.rowClicked.emit(row_id);
  }

  convertToDataTable(){
    setTimeout(()=>{
      $.fn.dataTable.ext.errMode = 'none';
      $('table.list-table').DataTable( {
          "order": [[ 1, "desc" ]],
          searching: false,
          paging: false,
          bFilter: false,
          bInfo: false,
          "columnDefs": [ {
            "targets": 0,
            "orderable": false
          } ]
      });
    },500);
  }

}
