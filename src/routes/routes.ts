import {  Routes } from '@angular/router';
import { LoginComponent } from '../modules/auth/login/login.component';
import { PatientProfileComponent } from '../modules/patient/profile/profile.component';
import { PatientSearchComponent } from '../modules/patient/search/search.component';
import { PatientHistoryComponent } from '../modules/patient/history/history.component';

import { EyeComponent } from '../modules/exam/eye/eye.component';
import { NewExamComponent } from '../modules/exam/new/new.component';
import { ContactLensComponent } from '../modules/exam/contact-lens/contact-lens.component';
import { ExamHistoryComponent } from '../modules/exam/history/history.component';
import { NewInventoryComponent } from '../modules/inventory/new/new.component';

import { SearchInventoryComponent } from '../modules/inventory/search/search.component';
import { InventoryDetailsComponent } from '../modules/inventory/details/details.component';
import { NewOrderComponent } from '../modules/order/new/new.component';
import { LightOrderComponent } from '../modules/order/light/light.component';
import { OrderSearchComponent } from '../modules/order/search/search.component';
import { OrderHistoryComponent } from '../modules/order/history/history.component';
import { OrderEditComponent } from '../modules/order/edit/edit.component';
import { ReceiptComponent } from '../modules/order/reciept/receipt.component';
import { CashBookComponent } from '../modules/finance/cashbook/cashbook.component';


export class RoutesHelper{

  static routes:Routes=[
    {
      path: "login",
      component: LoginComponent,
      data: { title: 'Login Page' , module:"auth"}
    },
    {
      path: "patient",
      component: PatientProfileComponent,
      data: { title: 'New Patient' }
    },
    {
      path: "patient/search",
      component: PatientSearchComponent,
      data: { title: 'Patient Search' }
    },
    {
      path: "patient/:id",
      component: PatientHistoryComponent,
      data: { title: 'Patient Profile' }
    },
    {
      path: "newExam",
      component: NewExamComponent,
      data: { title: 'Select Patient For Examination' }
    },
    {
      path: "eyeExam/:id/:label",
      component: EyeComponent,
      data: { title: 'Eye Exam' }
    },
    {
      path: "lensExam/:id/:label",
      component: ContactLensComponent,
      data: { title: 'Contact Lens Exam' }
    },
    {
      path: "examHistory/:id",
      component: ExamHistoryComponent,
      data: { title: 'Examination History' }
    },
    {
      path: "inventory",
      component: NewInventoryComponent,
      data: { title: 'Add Inventory' }
    },
    {
      path: "inventory/search",
      component: SearchInventoryComponent,
      data: { title: 'Search Inventory' }
    },
    {
      path: "inventory/:type/:id",
      component: InventoryDetailsComponent,
      data: { title: 'Inventory Details' }
    },
    {
      path: "order/search",
      component: OrderSearchComponent,
      data: { title: 'Orders List' }
    }
    ,
    {
      path: "order",
      component: NewOrderComponent,
      data: { title: 'Select Patient - New Order' }
    },
    {
      path: "order/:id",
      component: LightOrderComponent,
      data: { title: 'New Order',order:true }
    },
    {
      path: "order_exam/:id/:exam_id",
      component: LightOrderComponent,
      data: { title: 'New Order' }
    },
    {
      path: "orderHistory/:id",
      component: OrderHistoryComponent,
      data: { title: 'Orders History' }
    },
    {
      path: "order/edit/:id",
      component: OrderEditComponent,
      data: { title: 'Order Edit',order:true }
    },
    {
      path: "receipt/:order_id/:receipt_id",
      component: ReceiptComponent,
      data: { title: 'Receipt Details'}
    },
    {
      path: "cashbook",
      component: CashBookComponent,
      data: { title: 'CashBook'}
    }
    ,
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    { path: '**', component: LoginComponent }
  ]
}
