import { ElementFactory } from '../../factories/element-factory/element-factory';
import { Order } from './order';
export class SearchOrder{

    getlistConfig(fn){
      return {
        dataSource:"order/search",
        colsNames:["Order Number","Order Date","Patient Name","Date Of Birth","Phone","Order Status","Insurance Status","Delivered Date"],
        fieldsNames:["order_number","order_date","patient_name","dob","mobile","order_status","insurance_status","delivered_date"],
        searchObject:{},
        fn:fn
      }
    }

    getSearchFormConfig(fn1,fn2){
      return {
        "props":{
          id:"order_search"
        },
        "accordions":[
          {
            title:"Orders Search",
            rows:[
              [
                ElementFactory.generateCustomInput("tag","order_tags","Wild Search",6,-1),
                ElementFactory.generateButton([
                  {id:"search",icon:"search",text:"Search"},
                  {id:"order_status",icon:"search",text:"Order Status",custom:true,fn:fn1},
                  {id:"insurance_status ",icon:"search",text:"Insurance Status",custom:true,fn:fn2}
                ])
              ]
            ]
          }
        ]
      }
    }

    getOrderStatusPopConfig(){
      let orderConfig = new Order();
      let options = orderConfig.order_status_options.map(item=>{
        return {text:item.text,value:item.value}
      })
      return {
      "props":{
        id:"order_status_popup"
      },
      "accordions":[
        {
          title:"Order Status Search",
          rows:[
            [
              ElementFactory.generateCheckbox("order_status","Order Status",12,options)
            ],
            [
              ElementFactory.generateButton([{id:"search",icon:"search",text:"Search"}])
            ]
          ]
        }
      ]
    }
  }

  getInsuranceStatusPopConfig(){
    let orderConfig = new Order();
    let options = orderConfig.insurance_status_options.map(item=>{
      return {text:item.text,value:item.value}
    })
    return {
    "props":{
      id:"insurance_status_popup"
    },
    "accordions":[
      {
        title:"Insurance Status Search",
        rows:[
          [
            ElementFactory.generateCheckbox("insurance_status","Order Status",12,options)
          ],
          [
            ElementFactory.generateButton([{id:"search",icon:"search",text:"Search"}])
          ]
        ]
      }
    ]
  }
}

}
