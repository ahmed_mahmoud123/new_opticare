import { ElementFactory } from '../../factories/element-factory/element-factory';
import { Patient } from '../patient';
import { ServiceLocator } from '../../providers/service-locator';
import { GenericService } from '../../providers/generic.service';

export class Order{

  print_detailed_order_btn;
  print_summary_order_btn;
  payment_methods: Array<any> =[
      {text:"No Payment",value:"No Payment"},
      {text:"Insurance",value:"Insurance"},
      {text:"Cash",value:"Cash"},
      {text:"Mpesa",value:"Mpesa"},
      {text:"Card",value:"Card"},
      {text:"Cheque",value:"Cheque"},
      {text:"Airtel",value:"Airtel"},
      {text:"Bank transfer",value:"Bank"},
      {text:"Gift voucher",value:"Voucher"}
  ];
  order_status_options: Array<any> =[
    {value:"in progress",text:"In Progress"},
    {value:"on hold / reserve",text:"On hold / Reserve"},
    {value:"awaiting frame",text:"Awaiting Frame"},
    {value:"awaiting lenses locally ",text:"Awaiting Lenses Locally "},
    {value:"awaiting lenses overseas",text:"Awaiting Lenses Overseas"},
    {value:"glazing",text:"Glazing"},
    {value:"ready for collection",text:"Ready For Collection"},
    {value:"collected",text:"Collected"},
    {value:"delayed",text:"Delayed"},
    {value:"remake",text:"Remake"}
  ];
  insurance_status_options: Array<any> =[
    {value:"awaiting insurance approval",text:"Awaiting Insurance Approval"},
    {value:"insurance approval, waiting for smart",text:"Insurance Approval, Waiting For Smart"},
    {value:"insurance approval off smart",text:"Insurance Approval Off Smart"},
    {value:"claim complete ",text:"Claim complete "}
  ];
  // First accordion
  order_number:any = ElementFactory.generateInput("order_number","Order Number",4);
  order_date:any = ElementFactory.generateCustomInput("dob","order_date","Order Date",4);
  dispensed_by:any = ElementFactory.generateSelect("dispensed_by","Dispensed By",4,["-"],undefined,"-");
  delivered_date:any = ElementFactory.generateCustomInput("dob","delivered_date","Expected Delivery Date",4);
  patient_type = ElementFactory.generateRadio("p_type","Patient Type",4,[{text:"Private Patient",value:"private"},{text:"Insurance",value:"insurance"}],undefined,"private");
  // Second accordion
  patient_id:any = ElementFactory.generateInput("patient_id","Patient ID",4);
  patient_name:any = ElementFactory.generateInput("patient_name","Patient Name",4);
  dob:any = ElementFactory.generateInput("dob","Date Of Birth",4);
  mobile:any = ElementFactory.generateInput("mobile","Mobile",4);
  email:any = ElementFactory.generateInput("email","Email",4);
  // Fourth accordion
  amount_paid:any = ElementFactory.generateInput("amount_paid","Amount Paid",4,-1,"0");
  total_amount:any = ElementFactory.generateInput("total_amount","Total Amount",4,-1,"0");
  comment_method_1:any = ElementFactory.generateInput("comment_method_1","Comment",4);
  comment_method_2:any = ElementFactory.generateInput("comment_method_2","Comment",4);

  payment1:any = ElementFactory.generateSelect("payment1","Payment Method 1",4,this.payment_methods,undefined,"-");
  payment2:any = ElementFactory.generateSelect("payment2","Payment Method 2",4,this.payment_methods,undefined,"-");
  deposit_amount1:any = ElementFactory.generateInput("deposit_amount1","Deposit Amount 1",4,-1,"-");
  deposit_amount2:any = ElementFactory.generateInput("deposit_amount2","Deposit Amount 2",4,-1,"-");
  balance_amount:any = ElementFactory.generateInput("balance_amount","Balance Amount",4);

  order_status:any = ElementFactory.generateSelect("order_status","Order Status",4,this.order_status_options,undefined,"In Progress");
  insurance_status:any = ElementFactory.generateSelect("insurance_status","Insurance Status",4,this.insurance_status_options,undefined,"Awaiting Insurance Approval");

  order_notes:any=ElementFactory.generateCustomInput("textarea","order_notes","Order Notes",12);

  payment_summary:any;
  btns_bar = ElementFactory.generateButton([
    {id:"save",text:"Save"}
  ]);
  //
  config:any;
  final_prespective:any={};
  product_details:any={};
  poulatePatientData(p_id,group){
    Patient.getPatientData(p_id).then(data=>{
      if(data){
        group.controls["patient_id"].setValue(data._id);
        group.controls["patient_name"].setValue(data.first_name+" "+data.last_name);
        group.controls["dob"].setValue(data.dob);
        group.controls["mobile"].setValue(data.mobile);
        group.controls["email"].setValue(data.email);
      }
    });
  }


  constructor(fn1 = undefined,fn2 = undefined,self=undefined){
    //this.print_detailed_order_fn = fn1;
    this.print_detailed_order_btn = {classes:['btn blue'],custom:true,id:"print_detailed_order",text:"Print Detailed Order",fn:fn1,self:self};
    this.print_summary_order_btn = {classes:['btn blue'],custom:true,id:"print_summary_order",text:"Print Summary Order",fn:fn2,self:self};

    let now = new Date();
    this.order_date.value = `${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;
    this.delivered_date.value = this.getDeliveryDate(this.order_date.value);

    this.order_number.readOnly =true;
    this.patient_id.readOnly =true;
    this.patient_name.readOnly =true;
    this.email.readOnly =true;
    this.dob.readOnly =true;
    this.mobile.readOnly =true;
    this.final_prespective = {
      title:"Final RX",
      "aria_expanded":"collapsed",
      rows:[
        [
          ElementFactory.generateCheckbox("p_recommendation","Product Recommendation",12,[{text:"Distance",value:"dist",selected:false},{text:"Bifocals",value:"bifocals",selected:false},{text:"Reading / Near",value:"near",selected:false},{text:"Varifocals / Progressives",value:"vari_focals",selected:false},{text:"Contact Lens",value:"contact_lens",selected:false},{text:"Occupational",value:"occupational",selected:false},{text:"Intermediate",value:"Intermediate",selected:false}])
        ],
        [
          {type:"exam-history",accordions:["Final Prescription"]}
        ]
      ]
    }
    this.payment_summary = {type:"payment-summary"}
    this.product_details = {
      //title:"Product Details",
      rows:[
        [
          {type:"order-cart",cart:[]}
        ]
      ]
    }

    this.config = {
      props:{
        action : "order/new",
        edit:false,
        id:"order",
        msg:"Order data was added successfully .",
        preventClear:true,
        redirectUrl:"order/search"
      },
      accordions:[
        // First accordion
        {
          title:"Order Details",
          rows:[
            [
              this.order_number,
              this.order_date,
              this.dispensed_by
            ],[
              this.delivered_date,
              this.patient_type
            ]
          ]
        },
        // Second accordion
        {
          title:"Patient Details",
          rows:[
            [
              this.patient_id,
              this.patient_name,
              this.dob
            ],[
              this.mobile,
              this.email
            ]
          ]
        },
        // Third accordion
        this.final_prespective,
        // Fourth accordion
        this.product_details,
        // Fiveth accordion
        {
          title:"Billing Details",
          //"aria_expanded":"collapsed",
          rows:[
            [
              this.total_amount,
              this.amount_paid,
              this.balance_amount
            ],[
              this.payment1,
              this.deposit_amount1,
              this.comment_method_1
            ],[
              this.payment2,
              this.deposit_amount2,
              this.comment_method_2
            ],
            [
                this.order_status,
                this.insurance_status,
            ],
            [
              this.order_notes
            ],
            [
              this.btns_bar
            ],
            [this.payment_summary]
          ]
        }
      ]
    }
  }

  getDeliveryDate(order_date){
    let parts = order_date.split('/');
    let delivered_date = new Date(parts[2],parts[1]-1,parts[0]);
    delivered_date.setDate(delivered_date.getDate()+14);
    return `${delivered_date.getDate()}/${delivered_date.getMonth()+1}/${delivered_date.getFullYear()}`;
  }

  getOrderNumber(group){
    let __genericService = ServiceLocator.injector.get(GenericService);
    __genericService.get("order/max_order_number").then(data=>{
      if(data){
        var order_number:any = parseInt(data.order_number);
        if(isNaN(order_number)){
          order_number = 0;
          console.log("NAN");
        }
        order_number++;
        this.order_number.value = order_number;
      }else{
        this.order_number.value = "1";
      }
      console.log(this.order_number.value);
      group.controls["order_number"].setValue(this.order_number.value);
    });
  }

  getEditConfig(id){
    let config = Object.create(this.config);
    config.accordions[2].rows[1][0].edit = true;
    config.props = this.props(id);
    return config;
  }

  private props(id){
    return {
      fullUrl:true,
      action : "order/update",
      id:"order_edit",
      msg:"Order data was updated successfully .",
      edit:true,
      byIdUrl:"order/get/"+id,
      updated_id : id
    }
  }

  add_print_btns(){
    this.btns_bar.buttons.push(this.print_detailed_order_btn);
    this.btns_bar.buttons.push(this.print_summary_order_btn);

  }
}
