import { ElementFactory } from '../factories/element-factory/element-factory';
import {FormControl} from '@angular/forms';
import { Patient } from './patient';
export class BasicExam{

  protected config:any;
  protected exam_type:string;
  sph_values:Array<string>=[];
  cyl_values:Array<string>=[];
  axis_values:Array<string>=[];
  mono_pd_values:Array<string>=[];
  mono_npd_values:Array<string>=[];
  examination_date:string;
  base_curve:Array<string>=[];
  diameter:Array<string>=[];
  add:Array<string>=[];
  near_add:Array<string>=[];
  add_type = ["low","medium","high"];
  public final_prespective:any;
  patient:any;
  constructor(){
    this.cyl_values = ["DS"];
    for (let i = 30; i > 0; i-=0.25)
      this.sph_values.push("+"+i.toFixed(2));
    this.sph_values.push("0.00");
    this.sph_values.push("PLANO");
    for (let i = -0.25; i >= -30; i-=0.25)
      this.sph_values.push(i.toFixed(2));

    for (let i = -0.25; i >= -10; i-=.25)
      this.cyl_values.push(i.toFixed(2));

    for (let i = 1; i <= 180; i++)
      this.axis_values.push(i+"");
    for (let i = 20; i <= 40; i+=.5)
      this.mono_pd_values.push(i.toFixed(1)+"");
    for (let i = 20; i <= 40; i+=.25)
      this.mono_npd_values.push(i.toFixed(1)+"");
    for (let i = 7; i <= 9; i+=.1)
      this.base_curve.push(i.toFixed(1)+"");
    for (let i = 13; i < 20; i+=.1)
      this.diameter.push(i.toFixed(1)+"");
    for (let i = .25; i <= 6; i+=.25)
      this.add.push("+"+i.toFixed(2));
    for (let i = .25; i <= 6; i+=.25)
      this.near_add.push("+"+i.toFixed(2));
    let now = new Date();
    this.examination_date=`${now.getDate()}/${now.getMonth()+1}/${now.getFullYear()}`;
    this.overviewAccordion={
      title:"Overview",
      rows:[
        [
          ElementFactory.generateLabel("Patient Name",4,""),
          ElementFactory.generateLabel("Date Of Birth",4,""),
          ElementFactory.generateLabel("Examination Label",4,"")
        ],[
          ElementFactory.generateSelect('optometrist',"Optometrist",4,[{text:"ahmed",value:"ahmed"}],undefined,"-",undefined,true,["not_empty"]),
          ElementFactory.generateCustomInput('dob','examination_date',"Examination Date",4,1,this.examination_date,true,["required"])
        ]
      ]
    }
  }



  overviewAccordion:any;
  btns_bar:any= ElementFactory.generateButton([{id:"save",text:"Save Only"},{id:"save_exam_order",text:"Save and Order"},{id:"print",text:"Print",custom:true}]);

  otherDetailsAccordion:any={
    title:"Require Spectacles For",
    rows:[
      [
        ElementFactory.generateCheckbox("p_recommendation","Product Recommendation",12,[{text:"Distance",value:"dist",selected:false},{text:"Reading / Near",value:"near",selected:false},{text:"Intermediate",value:"intermediate",selected:false},{text:"Bifocals",value:"bifocals",selected:false},{text:"Multifocals",value:"vari_focals",selected:false},{text:"Suitable for Contact Lenses",value:"contact_lens",selected:false}])
      ],[
        ElementFactory.generateCustomInput("textarea","internal_notes","Internal Notes",6,27),
        ElementFactory.generateCustomInput("textarea","advice_given","Advice Given",6,28)
      ],
      [
        this.btns_bar
      ]
    ]
  }

  setPrintfn(fn){
    this.btns_bar.buttons[2].fn = fn;
  }
  public setPaitentName(name:string){
    this.overviewAccordion.rows[0][0].value=name;
  }

  public setPaitentDOB(name:string){
    this.overviewAccordion.rows[0][1].value=name;
  }

  public setLabel(name:string){
    this.overviewAccordion.rows[0][2].value=name;
  }


  // Fourth accordion
  recallAccordion:any={
    title:"Recall Details",
    rows:[
      [
        {
          type:"horizontal",
          col:6,
          rows:[
            ElementFactory.generateReExaminationSelect(),
            this.getRexamAdvise()
          ]
        },
          ElementFactory.generateCheckbox("recall_details","",6,[{text:"No Ocular Pathology",value:"1",selected:false},{text:"Ocular Pathology being managed in practice",value:"2",selected:false},{text:"No Prescription was Required",value:"3",selected:false},{text:"An unchanged prescription was issued",value:"4",selected:false},{text:"A new or changed prescription was issued",value:"5",selected:false}])
      ]
    ]
  };

  getRexamAdvise(){
    let elem = ElementFactory.generateInput("reExamination_advised","Re-examination Advised",8,-1,this.setReExam(12))
    elem.readOnly = true;
    return elem;
  }
  setReExam(num:number) {
    var d = new Date();
    var years = Math.floor(num / 12);
    var months = num - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return `${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;;
  }

  getPatientData(params,group){
    let promise = Patient.getPatientData(params['id']);
    promise.then(data=>{
      console.log(data);
      if(data){
        this.patient = data;
        group.addControl("patient_id",new FormControl(data._id,[]));
        group.addControl("exam_type",new FormControl(this.exam_type,[]));
        group.addControl("exam_label",new FormControl(params['label'],[]));
        this.setPaitentName(data.first_name+" "+data.last_name);
        this.setPaitentDOB(data.dob);
        this.setLabel(params['label']);
        return data;
      }
    });
    return promise;
  }

  setPatient(patient:any){
    this.setPaitentName(`${patient.title} ${patient.first_name} ${patient.last_name}`);
    this.setPaitentDOB(patient.dob);
  }

  generateEditConfig(id){
    let editConfig = this.config;
    editConfig.props = this.props(id);
    this.otherDetailsAccordion.rows[2][0].buttons[0].text = "Update";
    this.otherDetailsAccordion.rows[2][0].buttons[1].text = "Order";
    return editConfig;
  }

  setOrderFn(fn,self){
    this.otherDetailsAccordion.rows[2][0].buttons[1].fn = fn;
    this.otherDetailsAccordion.rows[2][0].buttons[1].self = self;
  }

  protected props(id){
    return {
      fullUrl:true,
      action : "exam/update",
      id:"exam_edit",
      msg:"Exam data was updated successfully .",
      edit:true,
      byIdUrl:"exam/get/"+id,
      updated_id : id
    }
  }
}
