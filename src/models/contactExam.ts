import { ElementFactory } from '../factories/element-factory/element-factory';
import { TableFactory } from '../factories/element-factory/table-factory';
import { HelperMethods } from '../providers/helper.services';
import { BasicExam } from './basicExam';


export class ContactExam extends BasicExam {

    table1_row1:any;
    table1_row2:any;
    table2_row1:any;
    table2_row2:any;
    table3_row1:any;
    config:any;
    protected print_fn;

  constructor(print_fn){
    super();
    this.print_fn = print_fn;
    this.exam_type="contact_exam";
    this.otherDetailsAccordion.title = "Require Contact Lens For";
    // Table 1 Row 1
    this.table1_row1=[
      "RE",
      ElementFactory.generateSelect("base_curve","",-1,this.base_curve,undefined,"-"),
      ElementFactory.generateSelect("diameter","",-1,this.diameter,undefined,"-"),
      ElementFactory.generateSelect("sph","",-1,this.sph_values,undefined,"-",undefined,true,["table_required"]),
      ElementFactory.generateSelect("cyl","",-1,this.cyl_values,undefined,"-"),
      ElementFactory.generateSelect("axis","",-1,this.axis_values,undefined,"-"),
      ElementFactory.generateSelect("add","",-1,this.add,undefined,"-"),
      ElementFactory.generateSelect("add_type","",-1,this.add_type,undefined,"-")
    ]
    // Table 1 Row 2
    this.table1_row2= [
      "LE",
      ElementFactory.generateSelect("l_base_curve","",-1,this.base_curve,undefined,"-"),
      ElementFactory.generateSelect("l_diameter","",-1,this.diameter,undefined,"-"),
      ElementFactory.generateSelect("l_sph","",-1,this.sph_values,undefined,"-",undefined,true,["table_required"]),
      ElementFactory.generateSelect("l_cyl","",-1,this.cyl_values,undefined,"-"),
      ElementFactory.generateSelect("l_axis","",-1,this.axis_values,undefined,"-"),
      ElementFactory.generateSelect("l_add","",-1,this.add,undefined,"-"),
      ElementFactory.generateSelect("l_add_type","",-1,this.add_type,undefined,"-")
    ]
    // Table 2 Row 1
    this.table2_row1=[
      "RE",
      ElementFactory.generateInput("D_V_A","",-1,2,"-",undefined,undefined,undefined,"10%"),
      HelperMethods.addProperties(ElementFactory.generateInput("D_Binoc","",-1,3,"-",undefined,undefined,undefined,"10%"),{rowspan:2}),
      ElementFactory.generateInput("N_V_A","",-1,4,"-",undefined,undefined,undefined,"10%"),
      HelperMethods.addProperties(ElementFactory.generateInput("N_Binco","",-1,5,"-",undefined,undefined,undefined,"10%"),{rowspan:2}),
      ElementFactory.generateInput("CL_Brand","",-1,6,"",true,["required"]),
      ElementFactory.generateInput("CL_Name","",-1,7,"",true,["required"]),
      ElementFactory.generateInput("CL_Color","",-1,8,"-")
    ];
    // Table 2 Row 2
    this.table2_row2 =[
      "LE",
      ElementFactory.generateInput("l_D_V_A","",-1,9,"-"),
      ElementFactory.generateInput("l_N_V_A","",-1,10,"-"),
      ElementFactory.generateInput("l_CL_Brand","",-1,11,"",true,["required"]),
      ElementFactory.generateInput("l_CL_Name","",-1,12,"",true,["required"]),
      ElementFactory.generateInput("l_CL_Color","",-1,13,"-")
    ]

    // Table 3 Row 1
    this.table3_row1 =[
      "",
      ElementFactory.generateSelect("wearing_mode","",-1,["DAILY","2 WEEKLY","MONTHLY","3 MONTHLY","YEARLY"],undefined,"DAILY",undefined,true,["table_required"]),
      ElementFactory.generateInput("days","",-1,14,"-"),
      ElementFactory.generateInput("hours","",-1,15,"-"),
      ElementFactory.generateInput("solution","",-1,16,"-"),
    ]
    this.final_prespective = {
      "title":"Final Prescription",
      "rows":
      [
        [
          TableFactory.generateTable(12,["","Base Curve","Diameter","Sph","Cyl","Axis","Add","Add Type"]
          ,[this.table1_row1,this.table1_row2])
        ],
        [
          TableFactory.generateTable(12,["","D V/A","D Binoc","N V/A","N Binco","CL Brand","	CL Name","CL Color"]
          ,[this.table2_row1,this.table2_row2],"second_table_comp")
        ],
        [
          TableFactory.generateTable(12,["Wearing Mode","Days","Hours","Solution"],[this.table3_row1])
        ]
      ]
    }
    this.config ={
      "props":{
        action : "exam/new",
        edit:false,
        id:"contactExam",
        msg:"Contact Exam data was added successfully ."
      },
      "accordions":[
        this.overviewAccordion,
        // First accordion
        this.final_prespective
        ,
      this.recallAccordion,
      this.otherDetailsAccordion
    ]
  }
  }


}
