import { ElementFactory } from '../../factories/element-factory/element-factory';

export class Cart{

  eye_exam_optios = [{text:"STANDARD",value:"STANDARD"},{text:"RECHECK",value:"RECHECK"}];
  contact_exam_optios = [{text:"CHECKUP",value:"CHECKUP"},{text:"TEACH",value:"TEACH"},{text:"FITTING",value:"FITTING"}];

  fees_config:any;

  constructor(){
    this.fees_config = {
      props:{},
      accordions:[
        // First accordion
        {
          title:"Add Examination Fees",
          "aria_expanded":"collapsed",
          rows:[
            [
              ElementFactory.generateSelect("e_type","Exam Type",6,[{text:"Eye Exam",value:"eye_exam"},{text:"Contact Lens Exam",value:"contact_exam"}],undefined,"eye_exam"),
              ElementFactory.generateSelect("e_label","Exam Label",6,this.eye_exam_optios,undefined,"STANDARD")
            ],[
              ElementFactory.generateInput("cost","Cost",6,-1,"1000")
            ],[
              ElementFactory.generateButton([{id:"add_fee",text:"Add Fee"}])
            ]
          ]
        }
      ]
    }
  }

  changeValues(e_type){
    if(e_type == "eye_exam"){
      this.fees_config.accordions[0].rows[0][1].options=this.eye_exam_optios;
    }else if(e_type == "contact_exam"){
      this.fees_config.accordions[0].rows[0][1].options=this.contact_exam_optios;
    }
  }
}
