import { ElementFactory } from '../factories/element-factory/element-factory';
import { TableFactory } from '../factories/element-factory/table-factory';
import { HelperMethods } from '../providers/helper.services';
import { BasicExam } from './basicExam';
export class EyeExam extends BasicExam{

    table1_row1:any;
    table1_row2:any;
    table2_row1:any;
    table2_row2:any;
    table3_row1:any;
    table3_row2:any;
    table4_row1:any;
    table4_row2:any;
    config:any;
  constructor(){
    super();
    this.exam_type="eye_exam";
    let pd = HelperMethods.addProperties(ElementFactory.generateInput("pd","",-1,1,"-"),{rowspan:2});
    pd.readOnly = true;
    let near_pd= HelperMethods.addProperties(ElementFactory.generateInput("near_pd","",-1,2,"-"),{rowspan:2});
    near_pd.readOnly = true;
    // Table 1 Row 1
    this.table1_row1=[
      "RE",
      ElementFactory.generateSelect("sph","",-1,this.sph_values,undefined,"-",undefined,true,["table_required"]),
      ElementFactory.generateSelect("cyl","",-1,this.cyl_values,undefined,"-"),
      ElementFactory.generateSelect("axis","",-1,this.axis_values,undefined,"-"),
      ElementFactory.generateSelect("mono_pd","",-1,this.mono_pd_values,undefined,"-",undefined,true,["table_required"]),
      pd,
      ElementFactory.generateSelect("mono_npd","",-1,this.mono_npd_values,undefined,"-"),
      near_pd
    ]
    // Table 1 Row 2
    this.table1_row2= [
      "LE",
      ElementFactory.generateSelect("l_sph","",-1,this.sph_values,undefined,"-",undefined,true,["table_required"]),
      ElementFactory.generateSelect("l_cyl","",-1,this.cyl_values,undefined,"-"),
      ElementFactory.generateSelect("l_axis","",-1,this.axis_values,undefined,"-"),
      ElementFactory.generateSelect("l_mono_pd","",-1,this.mono_pd_values,undefined,"-",undefined,true,["table_required"]),
      ElementFactory.generateSelect("l_mono_npd","",-1,this.mono_npd_values,undefined,"-"),
    ]
    // Table 2 Row 1
    this.table2_row1=[
      "RE",
      ElementFactory.generateSelect("near_add","",-1,this.near_add,undefined,"-"),
      ElementFactory.generateSelect("inter_add","",-1,this.near_add,undefined,"-"),
      ElementFactory.generateInput("vision_d","",-1,1,"-"),
      ElementFactory.generateInput("va","",-1,2,"-"),
      ElementFactory.generateInput("va_n","",-1,2,"-"),
      ElementFactory.generateInput("bvd","",-1,2,"-")
    ];
    // Table 2 Row 2
    this.table2_row2 =[
      "LE",
      ElementFactory.generateSelect("l_near_add","",-1,this.sph_values,undefined,"-"),
      ElementFactory.generateSelect("l_inter_add","",-1,this.sph_values,undefined,"-"),
      ElementFactory.generateInput("l_vision_d","",-1,3,"-"),
      ElementFactory.generateInput("l_va","",-1,4,"-"),
      ElementFactory.generateInput("l_va_n","",-1,2,"-"),
      ElementFactory.generateInput("l_bvd","",-1,2,"-")

    ]
    // Table 3 Row 1
    this.table3_row1=[
      "RE",
      ElementFactory.generateInput("hprism_d_prism","",-1,5,"-"),
      ElementFactory.generateInput("hbase_d_prism","",-1,6,"-"),
      ElementFactory.generateInput("vprism_d_prism","",-1,7,"-"),
      ElementFactory.generateInput("vbase_d_prism","",-1,8,"-")
    ];
    // Table 3 Row 2
    this.table3_row2 = [
      "LE",
      ElementFactory.generateInput("l_hprism_d_prism","",-1,10,"-"),
      ElementFactory.generateInput("l_hbase_d_prism","",-1,11,"-"),
      ElementFactory.generateInput("l_vprism_d_prism","",-1,12,"-"),
      ElementFactory.generateInput("l_vbase_d_prism","",-1,13,"-")
    ];

    // Table 4 Row 1
    this.table4_row1=[
      "RE",
      ElementFactory.generateInput("mf_add_prism","",-1,15,"-"),
      ElementFactory.generateInput("hprism_n_prism","",-1,16,"-"),
      ElementFactory.generateInput("hbase_n_prism","",-1,17,"-"),
      ElementFactory.generateInput("vprism_n_prism","",-1,18,"-"),
      ElementFactory.generateInput("vbase_n_prism","",-1,19,"-"),
      ElementFactory.generateInput("nva_prism","",-1,20,"-"),
    ];
    // Table 4 Row 2
    this.table4_row2 = [
      "RE",
      ElementFactory.generateInput("l_mf_add_prism","",-1,21,"-"),
      ElementFactory.generateInput("l_hprism_n_prism","",-1,22,"-"),
      ElementFactory.generateInput("l_hbase_n_prism","",-1,23,"-"),
      ElementFactory.generateInput("l_vprism_n_prism","",-1,24,"-"),
      ElementFactory.generateInput("l_vbase_n_prism","",-1,25,"-"),
      ElementFactory.generateInput("l_nva_prism","",-1,26,"-"),
    ];

    this.final_prespective = {
      "title":"Final Prescription",
      "rows":
      [
        [
          TableFactory.generateTable(12,["","Sph","Cyl","Axis","Mono PD","Dist PD","Mono NPD","Near PD"]
          ,[this.table1_row1,this.table1_row2])
        ],
        [
          TableFactory.generateTable(12,["","Near Add","Inter Add","Vision(D)","VA(D)","VA(N)","BVD"]
          ,[this.table2_row1,this.table2_row2])
        ]
      ]
    }

    this.config ={
      "props":{
        action : "exam/new",
        edit:false,
        id:"eyeExam",
        msg:"Eye Exam data was added successfully ."
      },
      "accordions":[
        this.overviewAccordion,
        // First accordion
        this.final_prespective
        ,
      // Second accordion
      {
        "title":"Prisms",
        "aria_expanded":"collapsed",
        "rows":
        [
          [
            TableFactory.generateTable(12,["","HPrism(D)","HBase(D)","Vprism(D)","Vbase(D)"]
            ,[this.table3_row1,this.table3_row2])
          ],
          [
            TableFactory.generateTable(12,["","MF Add","HPrism(N)","HBase(N)","Vprism(N)","Vbase(N)","NVA"]
            ,[this.table4_row1,this.table4_row2])
          ]
        ]
      },
      // Third accordion
      /*{
        title:"Require Spectacles For",
        rows:[
          [
            ElementFactory.generateRadio("spectacles_for","",12,[{text:"Distance & Near",value:"1"},{text:"Distance Only",value:"2"},{text:"Near Only",value:"3"},{text:"None",value:"4"}])
          ]
        ]
      }*/
      this.recallAccordion,
      this.otherDetailsAccordion
    ]
  }


  }


}
