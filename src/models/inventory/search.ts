import { ElementFactory } from '../../factories/element-factory/element-factory';

export class InventorySearch{

  public static searchListConfig(){
    let mappingValues={
      "0":"Frames",
      "1":"Sun Glass",
      "3":"Lens",
      "2":"Contact Lens",
      "4":"Accessories",
      "5":"Extras_lens"
    };
    return {
      dataSource:"inventory/search",
      colsNames:["Item Code","Type","Brand","Model","Color Code","Size","Price"],
      fieldsNames:["item_code","customType","brand","model","color_Code","size","selling_price"],
      searchObject:{},
      deleteUrl:"inventory/delete",
      mappingFn:function(doc){
        let brand = doc.brand? doc.brand.name : "",model=doc.model;
        if(doc.type == 2){
          brand = doc.cl_brand.name;
          model = doc.cl_name.name;
        }else if (doc.type == 3){
          brand = doc.lens_name.name;
        }
        doc.customType=mappingValues[doc.type];
        doc.brand=brand;
        doc.model=model;
        doc.size=doc.size;
        doc.color_code=doc.color_code;
        doc.selling_price=doc.selling_price;
        return doc;
      }
    }
  }

  public static popConfig:any={
    "props":{
      id:"inventory_popup_search"
    },
    "accordions":[
      {
        title:"Advanced Search",
        rows:[
          [
            ElementFactory.generateCheckbox("inventoyType","Inventory Type",12,[{text:"Frames",value:"1"},{text:"Sun Glass",value:"2"},{text:"Contact Lens",value:"3"},{text:"Lens",value:"4"},{text:"Accessories",value:"5"},{text:"Extras Lens",value:"6"}])
          ],
          [
            ElementFactory.generateCustomInput("tag","tags","Wild Search",9,-1)
          ],
          [
            ElementFactory.generateRange("priceRange","Price Range",9)
          ],
          [
            ElementFactory.generateButton([{id:"search",icon:"search",text:"Search"}])
          ]
        ]
      }
    ]
  }

}
