import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicInventory } from './basicInventory';


export class LensExtras extends BasicInventory{
  public static instance = undefined;

  protected extras = ElementFactory.generateSelect("extras","Extras",4,undefined,"extras/get","-",'extras/new',true,["not_empty"]);

  constructor(){
    super();
  }
  config:any={
    "props":{
      action : "inventory/new",
      edit:false,
      redirectUrl:"inventory/search",
      id:"lens_extras_inventory",
      msg:"Lens Extras Inventory data was added successfully ."
    },
    "accordions":[
      {
        "title":"Lens Extra",
        rows:[
          [
            this.item_code,
            this.extras,
            this.cost
          ],
          [
            this.selling_price
          ],
          [
            this.saveBtn
          ]
        ]
      }
    ]
  }
  public static getInstance(){
    if(LensExtras.instance == undefined){
      LensExtras.instance = new LensExtras();
    }
    return LensExtras.instance;
  }
}
