import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicInventory } from './basicInventory';
export class CLInventory extends BasicInventory{
  public static instance = undefined;

  protected cl_brand = ElementFactory.generateSelect("cl_brand","CL Brand",4,undefined,"cl_brand/get","-",'cl_brand/new',true,["required"]);
  protected cl_name = ElementFactory.generateSelect("cl_name","CL Name",4,undefined,"cl_name/get","-",'cl_name/new',true,["required"]);
  protected modality = ElementFactory.generateSelect("modality","Modality",4,[{text:"Daily",value:"1"},{text:"2 Weekly",value:"2"},{text:"Monthly",value:"3"},{text:"3 Monthly",value:"4"},{text:"Yearly",value:"5"}],undefined,"1",undefined,true,["required"]);
  protected add_type = ElementFactory.generateSelect("add_type","Add Type",4,[{text:"Low",value:"1"},{text:"Medium",value:"2"},{text:"High",value:"3"}],undefined,"-");

  constructor(){
    super();
    this.changeColNumber(3);
  }
  config:any={
    "props":{
      action : "inventory/new",
      edit:false,
      redirectUrl:"inventory/search",
      id:"cl_inventory",
      msg:"Contact Lens data was added successfully .",
      byIdUrl:"inventory/details/"
    },
    "accordions":[
      {
        "title":"Contact Lens",
        rows:[
          [
            this.item_code,
            this.supplier,
            this.cl_brand
          ],
          [
            this.cl_name,
            this.modality,
            this.base_curve
          ],
          [
            this.sph,
            this.cyl,
            this.axis

          ],[
            this.near_add,
            this.inter_add,
            this.add_type
          ],
          [
            this.reorder_level,
            this.quantity,
            this.cost,
            this.selling_price
          ],
          [
            this.saveBtn
          ]
        ]
      }
    ]
  }

  public static getInstance(){
    if(CLInventory.instance == undefined)
      CLInventory.instance = new CLInventory();
    return CLInventory.instance;
  }
}
