import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicExam } from '../basicExam';
import { ServiceLocator } from '../../providers/service-locator';
import { GenericService } from '../../providers/generic.service';

export class BasicInventory{
  private exam = new BasicExam();
  public config;
  protected inventory_type;
  protected item_code = ElementFactory.generateInput("item_code","Item Code",4,-1);
  protected supplier = ElementFactory.generateSelect("supplier","Supplier",4,undefined,"supplier/get","-",'supplier/new');
  protected reorder_level = ElementFactory.generateInput("reorder_level","Reorder Level",4,-1,"-");
  protected quantity = ElementFactory.generateInput("quantity","Quantity",4,-1,"-",true,["required"]);
  protected cost = ElementFactory.generateInput("cost","Cost",4,-1,"-");
  protected selling_price = ElementFactory.generateInput("selling_price","Selling Price",4,-1,"-",true,["required"]);
  protected saveBtn = ElementFactory.generateButton([{id:"save",text:"Save"}])
  protected base_curve = ElementFactory.generateSelect("base_curve","Base Curve",4,this.exam.base_curve,undefined,"-",undefined,true,["required"]);
  protected sph = ElementFactory.generateSelect("sph","Sph",4,this.exam.sph_values,undefined,"-",undefined,true,["required"]);
  protected cyl = ElementFactory.generateSelect("cyl","Cyl",4,this.exam.cyl_values,undefined,"-",undefined,true,["required"]);
  protected near_add = ElementFactory.generateSelect("near_add","Near Add",4,this.exam.near_add,undefined,"-");
  protected inter_add = ElementFactory.generateSelect("inter_add","Inter Add",4,this.exam.near_add,undefined,"-");
  protected axis = ElementFactory.generateSelect("axis","Axis",4,this.exam.axis_values,undefined,"-");

  changeColNumber(col){
    this.reorder_level.col = col;
    this.quantity.col = col;
    this.cost.col = col;
    this.selling_price.col = col;
  }
  constructor(){
    this.item_code.readOnly = true;
  }
  getItemCode(group){
    let __genericService = ServiceLocator.injector.get(GenericService);
    __genericService.get("inventory/max_item_code").then(data=>{
      if(data){
        var item_code:any = parseInt(data.item_code);
        if(isNaN(item_code))
          item_code = 0;
        item_code++;
        for (let i = item_code.toString().length; i < 5; i++) {
            item_code="0"+item_code;
        };
        this.item_code.value = item_code;
      }else{
        this.item_code.value = "00001";
      }
      group.controls['item_code'].setValue(this.item_code.value);
    });
  }

  getEditConfig(inventory_type=0){
    let config = this.config;
    config.props.byIdUrl="inventory/details/";
    config.props.msg=config.props.msg.replace("added", "updated");
    config.props.edit = true;
    config.props.action = "inventory/update";
    this.saveBtn.buttons[0].text = "Update";
    return config;
  }
}
