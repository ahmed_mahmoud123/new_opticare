import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicInventory } from './basicInventory';


export class LensInventory extends BasicInventory{
  public static instance = undefined;

  protected lens_type = ElementFactory.generateSelect("lens_type","Lens Type",4,[{text:"Single Vision",value:"single_vision"},{text:"Bifocal",value:"bitfocal"},{text:"Vari Focals / Progressives",value:"vari_focals_progrssives"}],undefined,"-",undefined,true,["required"]);
  protected material = ElementFactory.generateSelect("material","Materials",4,[{text:"Glass",value:"glass"},{text:"Plastic",value:"plastic"}],undefined,"-",undefined,true,["required"]);
  protected lens_name = ElementFactory.generateSelect("lens_name","Lens Name",4,undefined,"lens_name/get","-",'lens_name/new',true,["required"]);
  private index_values:Array<any> = ["1.50","1.55","1.59","1.60","1.67","1.74","1.7","1.8","1.9"];
  protected index = ElementFactory.generateSelect("index","Index",4,this.index_values,undefined,"-",undefined,true,["required"]);

  constructor(){
    super();
    this.changeColNumber(3);
  }
  config:any={
    "props":{
      action : "inventory/new",
      edit:false,
      redirectUrl:"inventory/search",
      id:"lens_inventory",
      msg:"Lens data was added successfully ."
    },
    "accordions":[
      {
        "title":"Lens",
        rows:[
          [
            this.item_code,
            this.supplier,
            this.lens_type
          ],
          [
            this.lens_name,
            this.index,
            this.material
          ],
          [
            this.sph,
            this.cyl,
            this.axis
          ],[
            this.near_add,
            this.inter_add
          ],
          [
            this.reorder_level,
            this.quantity,
            this.cost,
            this.selling_price
          ],
          [
            this.saveBtn
          ]
        ]
      }
    ]
  }

  public static getInstance(){
    if(LensInventory.instance == undefined)
      LensInventory.instance = new LensInventory();
    return LensInventory.instance;
  }
}
