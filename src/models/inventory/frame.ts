import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicInventory } from './basicInventory';

export class FrameInventory extends BasicInventory{
  public static instance = undefined;

  protected brand = ElementFactory.generateSelect("brand","Brand",4,undefined,"brand/get","-",'brand/new');
  protected model_name =  ElementFactory.generateInput("model","Model Name",4,1,"-");
  protected model_number =  ElementFactory.generateInput("model_number","Model Number",4,2,"-",true,["required"]);
  protected size =  ElementFactory.generateInput("size","Size",4,3,"-",true,["required"]);
  protected colour = ElementFactory.generateSelect("colour","Colour Name",4,undefined,"colour/get","-",'colour/new',true,["required"]);
  protected color_Code =  ElementFactory.generateInput("color_Code","Colour Code",4,3,"-",true,["required"]);
  protected gender = ElementFactory.generateRadio("gender","Gender",4,[{text:"Male",value:"male"},{text:"Female",value:"female"},{text:"Unisex",value:"unisex"}],undefined,"male");
  protected materials = ElementFactory.generateSelect("materials","Material",4,[{text:"PLASTIC",value:"plastic"},{text:"METAL",value:"metal"},{text:"TITANIUM",value:"titanium"}],undefined,"-",undefined,true,["required"]);
  protected frame_type = ElementFactory.generateSelect("frame_type","Frame Type",4,[{text:"Full Frame",value:"framed"},{text:"Semi Rimless/SUPRA",value:"semi_rimless"},{text:"Rimless",value:"rimless"}],undefined,"-",undefined,true,["required"]);
  protected spring_hinge = ElementFactory.generateRadio("spring_hinge","Spring Hinge",4,[{text:"Yes",value:"yes"},{text:"No",value:"no"}],undefined,"yes");

  constructor(){
    super();
  }
  config:any={
    "props":{
      action : "inventory/new",
      edit:false,
      redirectUrl:"inventory/search",
      id:"frame_inventory",
      msg:"Frame Inventory data was added successfully .",
      byIdUrl:"inventory/details/"
    },
    "accordions":[
      {
        "title":"Frames",
        rows:[
          [
            this.item_code,
            this.supplier,
            this.brand
          ],[
            this.model_name,
            this.model_number,
            this.size
          ],[
            this.colour,
            this.color_Code,
            this.gender
          ],[
            this.materials,
            this.frame_type,
            this.spring_hinge
          ],[
            this.reorder_level,
            this.quantity,
            this.cost
          ],[
            this.selling_price
          ],[
            this.saveBtn
          ]
        ]
      }
    ]
  }

  setConfig(inventory_type){
    switch(inventory_type){
      case "0":
        this.config.accordions[0].title="Frames";
        this.config.props.msg="Frame Inventory data was added successfully .";
        break;
      case "1":
        this.config.accordions[0].title="Sun Glass";
        this.config.props.msg="Sun Glass Inventory data was added successfully .";
        break;
    }

  }

  getEditConfig(inventory_type){
    this.setConfig(inventory_type);
    let config = this.config;
    config.props.msg=config.props.msg.replace("added", "updated");
    config.props.edit = true;
    config.props.action = "inventory/update";
    this.saveBtn.buttons[0].text = "Update";
    return config;
  }
  public static getInstance(){
    if(FrameInventory.instance == undefined)
      FrameInventory.instance = new FrameInventory();
    return FrameInventory.instance;
  }
}
