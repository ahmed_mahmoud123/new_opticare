import { ElementFactory } from '../../factories/element-factory/element-factory';
import { BasicInventory } from './basicInventory';


export class AccessoriesInventory extends BasicInventory{
  public static instance = undefined;

  protected category = ElementFactory.generateSelect("category","Category",4,undefined,"category/get","-",'category/new',true,["required"]);
  protected notes = ElementFactory.generateCustomInput("textarea","notes","Notes",8,1,"");

  constructor(){
    super();
  }
  config:any={
    "props":{
      action : "inventory/new",
      edit:false,
      redirectUrl:"inventory/search",
      id:"access_inventory",
      msg:"Accessories Inventory data was added successfully ."
    },
    "accordions":[
      {
        "title":"Accessories",
        rows:[
          [
            this.item_code,
            this.category,
            this.supplier
          ],
          [
            this.notes,
            this.quantity,
          ],
          [
            this.reorder_level,
            this.cost,
            this.selling_price
          ],
          [
            this.saveBtn
          ]
        ]
      }
    ]
  }
  public static getInstance(){
    if(AccessoriesInventory.instance == undefined){
      AccessoriesInventory.instance = new AccessoriesInventory();
    }
    return AccessoriesInventory.instance;
  }
}
