import { ElementFactory } from '../factories/element-factory/element-factory';
import { ServiceLocator } from '../providers/service-locator';
import { GenericService } from '../providers/generic.service';

export class Patient{
    patient_id:any=ElementFactory.generateInput("_id","Patient ID",4,1);
    title:any=ElementFactory.generateSelect("title","Title",4,[{text:"Mr.",value:"mr"},{text:"Mrs.",value:"mrs"},{text:"Ms.",value:"ms"},{text:"Dr.",value:"dr"},{text:"Mst.",value:"mst"}],undefined,"mr");
    geneder:any=ElementFactory.generateRadio("gender","Gender",4,[{text:"Male",value:"male"},{text:"Female",value:"female"}],undefined,"male");
    first_name:any=ElementFactory.generateInput("first_name","First Name",4,2,"",true,["required"]);
    middle_name:any=ElementFactory.generateInput("middle_name","Middle Name",4,3);
    last_name:any=ElementFactory.generateInput("last_name","Last Name",4,4,"",true,["required"]);
    dob:any=ElementFactory.generateCustomInput("dob","dob","Date of Birth",4,5,"",true,["required","valid_date"]);
    national_id:any=ElementFactory.generateInput("national_id","National ID",4,6);
    insured_patient:any=ElementFactory.generateRadio("insured","Insured Patient",4,[{text:"Yes",value:"1"},{text:"No",value:"0"}],undefined,"0");
    insurance_company_name:any=ElementFactory.generateInput("insurance_company_name","Insurance Company Name",4,-1,"",false,undefined,['insurance']);
    insurance_number:any=ElementFactory.generateInput("insurance_number","Insurance Number",4,-1,"",false,undefined,['insurance']);
    country:any=ElementFactory.generateSelect("country","Country",4,undefined,"country/get","5a19780e0b9c1d623dc856d5",'country/new');
    state:any=ElementFactory.generateSelect("state","State",4,undefined,"state/get","-",'state/new');
    city:any=ElementFactory.generateSelect("city","Town/City",4,undefined,"city/get","59cf4d602ca5c5736ceeace6",'city/new');
    district:any=ElementFactory.generateSelect("district","District",4,undefined,"district/get","-",'district/new');
    address:any=ElementFactory.generateInput("address","Address",4,7);
    postal_code:any=ElementFactory.generateInput("postal_code","Postal Code (ZIP/PIN)",4,8);
    mobile:any=ElementFactory.generateCustomInput("phone","mobile","Mobile",4,9,"",true,["required"]);
    home_phone:any=ElementFactory.generateCustomInput("phone","home_phone","Home Phone",4,10);
    work_phone:any=ElementFactory.generateCustomInput("phone","work_phone","Work Phone",4,11);
    email:any=ElementFactory.generateCustomInput("email","email","Email",4,12,"",true,["required","email"]);
    receive_methods:any=ElementFactory.generateCheckbox("receive_methods","Patient agrees to receive",8,[{text:"Phone",value:"phone",selected:true},{text:"Post",value:"post",selected:true},{text:"Sms",value:"sms",selected:true},{text:"E-mail",value:"email",selected:true}]);
    general_info:any=ElementFactory.generateCustomInput("textarea","info","General Info",12,13);
    // Form Model Config Object
    config:any;
    // List Config
    public static listConfig:any={
      dataSource:"patient/search",
      colsNames:["Patient ID","First Name","Last Name","Date Of Birth","Mobile","Email"],
      fieldsNames:["_id","first_name","last_name","dob","mobile","email"],
      searchObject:{},
      deleteUrl:"patient/delete"
    }

    generateEditFormConfig():any{
      let editConfig = this.config;
      editConfig.props.edit = true;
      editConfig.props.action = "patient/update";
      editConfig.props.id="patient_edit"
      editConfig.props.byIdUrl="patient/details/";
      editConfig.props.msg="Patient data was updated successfully ."
      editConfig.accordions[0].rows[0].unshift(this.patient_id);
      editConfig.accordions[2].rows[5].pop();
      let accordion = {"title":"Additional Information","rows":[]};
      accordion.rows.push([
        ElementFactory.generateRadio("p_status","Patient Satus",6,[{text:"Active",value:"active"},{text:"Deceased",value:"deceased"},{text:"Moved Away",value:"moved_away"}],undefined,"active"),
        ElementFactory.generateCheckbox("p_status_arr"," ",3,[{text:"Key Patient",value:"key_patient",selected:false},{text:"Member of Staff",value:"staff",selected:false},{text:"Staff Relative",value:"staff_relative",selected:false}])
      ]);
      accordion.rows.push([
        ElementFactory.generateButton([{id:"update",text:"Update"}])
      ])
      editConfig.accordions.push(accordion);
      return editConfig;
    }

    constructor(){
      this.insurance_company_name.readOnly = true;
      this.insurance_number.readOnly = true;
      this.patient_id.readOnly = true;
      this.config ={
        "props":{
          title : "The Patient consents to the use of personal data for the provision of optometric services and optical goods",
          action : "patient/new",
          edit:false,
          id:"patient",
          msg:"Patient data was added successfully .",
          redirectUrl:"patient/search"
        },
        "accordions":[{
          "title":"Personal Info",
          "rows":
          [
            [
              this.title,
              this.geneder
            ],
            [
              this.first_name,
              this.middle_name,
              this.last_name
            ],
            [
              this.dob,
              this.national_id
            ]
          ]
        },
        {
          "title":"Insurance Information",
          "rows":
          [
            [
              this.insured_patient,
              this.insurance_company_name,
              this.insurance_number
            ]
          ]
        },
        {
          "title":"Contact Info",
          "rows":
          [
            [
              this.country,
              this.state,
              this.city
            ],
            [
              this.district,
              this.address,
              this.postal_code
            ],
            [
              this.mobile,
              this.home_phone,
              this.work_phone
            ],
            [
              this.email,
              this.receive_methods
            ],
            [
              this.general_info
            ],
            [
              ElementFactory.generateButton([{id:"save",text:"Save Only"},{id:"save_examine",text:"Save & Examine"}])
            ]
          ],
        }
      ]
      }
    }

    public static getPatientData(id){
      let __genericService = ServiceLocator.injector.get(GenericService);
      return __genericService.get("patient/details/"+id);
    }
}
