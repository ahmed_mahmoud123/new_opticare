import { Component } from '@angular/core';


@Component({
  selector : "app-footer",
  templateUrl:"./footer.component.html",

})

export class FooterComponent{
  year :any = new Date().getFullYear();
  scripts:Array<string>=[
    "layouts/layout/scripts/layout.min",
    "datepicker-js/bootstrap-datepicker"]
  constructor(){
    this.scripts.forEach(script=>{
      this.loadScript("assets/"+script+".js");
    })
  }

  public loadScript(url) {
      let node = document.createElement('script');
      node.src = url;
      node.type = 'text/javascript';
      document.getElementsByTagName('head')[0].appendChild(node);
   }

}
