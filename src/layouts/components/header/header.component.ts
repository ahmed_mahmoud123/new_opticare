import { Component,OnInit } from '@angular/core';


@Component({
  selector : "app-header",
  templateUrl:"./header.component.html"
})

export class HeaderComponent implements OnInit {

  styles:Array<string>=[
    "global/plugins/font-awesome/css/font-awesome.min",
    "global/plugins/simple-line-icons/simple-line-icons.min",
    "global/plugins/bootstrap/css/bootstrap.min",
    "global/plugins/bootstrap-switch/css/bootstrap-switch.min",
    "global/plugins/datatables/datatables.min",
    "global/plugins/datatables/plugins/bootstrap/datatables.bootstrap",
    "global/plugins/select2/css/select2.min",
    "global/plugins/select2/css/select2-bootstrap.min",
    "global/css/components.min",
    "global/css/components-md.min",
    "global/css/plugins-md.min",
    "layouts/layout/css/layout.min",
    "layouts/layout/css/themes/darkblue.min",
    "layouts/layout/css/custom.min",
    "common",
    "global/css/cmxform",
    "pages/css/login.min"
  ]

  constructor(){}

  ngOnInit(){
    
  }

  public loadStyle(url) {
      let node = document.createElement('link');
      node.href = url;
      node.type = 'text/css';
      node.rel="stylesheet";
      document.getElementsByTagName('head')[0].appendChild(node);
   }

}
