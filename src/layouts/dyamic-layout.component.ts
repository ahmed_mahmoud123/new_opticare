import { Input,ComponentFactoryResolver , Component , ViewContainerRef ,OnChanges } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContentComponent } from './components/content/content.component';
import { MenuComponent } from './components/menu/menu.component';

const parts = {
  'header' : HeaderComponent,
  'footer' : FooterComponent,
  'content': ContentComponent,
  'menu': ContentComponent
}
@Component({
  selector:'dynamic-layout',
  templateUrl :'dynamic-layout.component.html'
})

export class DynamicLayoutComponent implements OnChanges{
  private templates:any = {
    'default' : [
      {name:"header"},
      {name:"content",withMenu:true},
      {name:"footer"}
    ],
    'auth':[
      {name:"content",withMenu:false}
    ]
  };
  private currentTemplate:string = "default";
  @Input() template:string;

  constructor(private resolver:ComponentFactoryResolver,private container:ViewContainerRef){}

  ngOnChanges(changes){
    if(changes.template && changes.template.currentValue != this.currentTemplate){
      console.log(changes.template.currentValue)
      this.currentTemplate = changes.template.currentValue;
      this.container.clear();
      if(this.templates[this.template])
      this.templates[this.template].forEach(template=>{
        let component=parts[template.name];
        let factory = this.resolver.resolveComponentFactory<any>(component);
        let currentComponent=this.container.createComponent(factory);
        if(template.name == "content")
          currentComponent.instance.withMenu = template.withMenu;
      });
    }
  }
}
