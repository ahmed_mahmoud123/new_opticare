import { NgModule } from '@angular/core';
import { DynamicLayoutComponent } from './dyamic-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContentComponent } from './components/content/content.component';
import { MenuComponent } from './components/menu/menu.component';
import { CommonModule } from '@angular/common';

import {  RouterModule,Routes } from '@angular/router';
import { RoutesHelper } from '../routes/routes';

@NgModule({
  declarations:[
    DynamicLayoutComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    MenuComponent
  ],
  exports:[
    DynamicLayoutComponent
  ],
  entryComponents:[
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    MenuComponent
  ],
  imports:[
      CommonModule,
      RouterModule.forRoot(RoutesHelper.routes),
  ]
})
export class DynamicLayoutModule {}
