import { FormControl } from '@angular/forms';


export class CustomValidator {

  constructor(){}

  static isEmpty(control: FormControl): any {
    if(control.value == "" || control.value == "-"){
        return { "not_empty": true };
    }
    return null;
  }

  static isTableRequired(control: FormControl): any {
    if(control.value == "" || control.value == "-"){
        return { "table_required": true };
    }
    return null;
  }

  static isDate(control: FormControl): any {
    if(control.value == "" || !control.value.match(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/)){
        return { "valid_date": true };
    }
    return null;
  }




}
