export class ValidatorMessages{
  public static messages:any={
    "required" : " Field is required ",
    "email": " Field must be valid email",
    "not_empty" : " Field is required ",
    "table_required":"Required",
    "valid_date":"Invalid date format DD/MM/YYYY"
  }
}
