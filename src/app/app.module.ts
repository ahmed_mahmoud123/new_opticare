import { BrowserModule,Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ServiceLocator } from '../providers/service-locator';
import {Injector} from "@angular/core";

import { AppComponent } from './app.component';

import { DynamicLayoutModule } from '../layouts/dynamic-layout.module';
import { AuthModule } from '../modules/auth/auth.module';
import { PatientModule } from '../modules/patient/patient.module';
import { ExamModule } from '../modules/exam/exam.module';
import { InventoryModule } from '../modules/inventory/inventory.module';
import { OrderModule } from '../modules/order/order.module';
import { FinanceModule } from '../modules/finance/finance.module';

import { PrintService } from '../providers/print.service';
import { GenericService } from '../providers/generic.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DynamicLayoutModule,
    PatientModule,
    ExamModule,
    AuthModule,
    InventoryModule,
    OrderModule,
    FinanceModule
  ],
  providers: [Title,GenericService,PrintService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    ServiceLocator.injector = this.injector;
  }
}
