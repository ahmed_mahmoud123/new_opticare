import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router , ActivatedRoute ,NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
declare var $:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  template:string;
  tabindex:any=0;
  constructor(private router:Router,private activatedRoute:ActivatedRoute,private title:Title){
    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => {
        window.scroll(0,0);
        this.title.setTitle(event['title']);
        this.tabindex = 0;
        setTimeout(()=>{
          $('[tabindex="1"]').focus();
        },500);
        if(event['module'] == 'auth'){
          this.template = "auth";
        }else{
          this.template = "default";
        }
      });
  }

  ngAfterViewInit(){
    /*
    $(document).on('keydown',(e)=>{
      if(e.which == 9 || e.which==39 || e.which==37){
        e.preventDefault();
        if($(e.target).attr("tabindex")){
          this.tabindex=Number($(e.target).attr("tabindex"));
        }
        if(e.which==39 || e.which == 9)
          ++this.tabindex;
        else if(e.which==37)
          --this.tabindex;
        if($('[tabindex=' + this.tabindex + ']')){
          $('[tabindex=' + this.tabindex + ']').focus();
          console.log('[tabindex=' + this.tabindex + ']');
        }else
          $('[tabindex="1"]').focus();
      }
    })
    */
  }
  public resetTabIndex(){
    this.tabindex = 0;
    setTimeout(()=>{
      $('[tabindex="1"]').focus();
    },500);
  }
}
