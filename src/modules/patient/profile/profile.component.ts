import { Component,Input,Output,OnInit,EventEmitter  } from '@angular/core';
import {Router } from '@angular/router';
import { Patient } from '../../../models/patient';

declare var $: any;

@Component({
  selector :'patient',
  templateUrl : './profile.component.html',
  styleUrls :['profile.component.scss']
})

export class PatientProfileComponent implements OnInit{
  patient:Patient=new Patient();
  title:string;
  @Input() isEdit:boolean=false;
  @Output() patientData = new EventEmitter();
  patient_id:number;
  config:any;
  constructor(private router:Router){}

  ngOnInit(){
    if(!this.isEdit){
      this.title = "New Patient";
      this.config = this.patient.config;
    }else
      this.config = this.patient.generateEditFormConfig();
  }

  formChange(data:any){
    $('.insurance').prop('readOnly',!parseInt(data.group.controls['insured'].value));
  }

  afterSubmited($event){
    if($event.source == "save_examine"){
      if($event._id){
        this.patient_id = $event._id;
        $('#modal_exam_type').modal();
      }
    }else{
      if($event._id)
        this.router.navigate(["/patient/"+$event._id]);
    }
  }

}
