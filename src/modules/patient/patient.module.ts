import { NgModule } from '@angular/core';
import { PatientProfileComponent } from './profile/profile.component';
import { PatientSearchComponent } from './search/search.component';
import { PatientHistoryComponent } from './history/history.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DynamicFormModule } from '../../factories/form-factory/dynamic-form.module';
import { DynamicListModule } from '../../factories/list-factory/dynamic-list.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations:[
    PatientProfileComponent,
    PatientSearchComponent,
    PatientHistoryComponent
  ],
  imports:[
    RouterModule,
    DynamicFormModule,
    DynamicListModule,
    CommonModule,
    SharedModule
  ],
  exports:[
    PatientSearchComponent
  ]
})

export class PatientModule {}
