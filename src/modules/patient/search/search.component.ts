import { Component,Input,Output,EventEmitter  } from '@angular/core';
import { Patient } from '../../../models/patient';
import { ElementFactory } from '../../../factories/element-factory/element-factory';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector:"patient-search",
  templateUrl : './search.component.html',
  styleUrls :['./search.component.scss']
})

export class PatientSearchComponent{
  @Input() title:string = "Patient Search";
  patient:Patient=new Patient();
  @Input() defaultClick:boolean = true;
  @Output() patientRowClicked:any = new EventEmitter();
  searchListConfig:any=Patient.listConfig;
  searchObject:any = {};
  searFormConfig:any = {
    props:{},
    accordions:[
      {
        title:"Search Patient Details",
        rows:[
          [
            ElementFactory.generateInput('_id',"Patient ID",4,1),
            ElementFactory.generateInput('first_name',"First Name",4,2),
            ElementFactory.generateInput('last_name',"Last Name",4,3)
          ],
          [
            ElementFactory.generateCustomInput('dob','dob',"Date Of Birth",4,4),
            ElementFactory.generateInput('mobile',"Mobile",4,5),
            ElementFactory.generateInput('national_id',"National ID",4,6),
          ],
          [
            ElementFactory.generateCheckbox('wild_search',"Wild Search",2,[{text:"",value:"",selected:false}]),
            ElementFactory.generateInput('keyword',".",8,7,"",false,undefined,['wild'])
          ],
          [
            ElementFactory.generateButton([{id:"search",text:"Search",icon:"search"}])
          ]
        ]
      }
    ]
  }
  constructor(private router:Router){}

  ngAfterViewInit(){
    $('.wild').prop('disabled',true);
  }
  search(event){
    delete event['source'];
    event.wild_search = event.wild_search[0];
    this.searchObject = event;
  }
  formChange(event){
    $('.wild').prop('disabled',!event.group.value.wild_search[0]);
  }
  patientClicked(patient){
    if(this.defaultClick){
      this.router.navigate(['/patient/'+patient._id]);
    }else{
      this.patientRowClicked.emit(patient._id);
    }
  }
}
