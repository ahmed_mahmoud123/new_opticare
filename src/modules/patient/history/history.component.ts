import { Component  } from '@angular/core';
import { Patient } from '../../../models/patient';
import { Title } from '@angular/platform-browser';

declare var $: any;

@Component({
  templateUrl : './history.component.html',
  styleUrls :['history.component.scss']
})

export class PatientHistoryComponent  {
  patientModel:Patient=new Patient();
  patient:any={};
  config:any;
  constructor(private title:Title){
    this.config = this.patientModel.config;
  }

  afterPatientDataBack(patientData){
    if(patientData){
      this.patient = patientData;
      this.title.setTitle(`Patient Profile - ${patientData.first_name} ${patientData.last_name}`);
      $('.insurance').prop('disabled',!parseInt(this.patient.insured));
    }
  }

}
