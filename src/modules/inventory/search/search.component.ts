import { Component,Input,OnInit,Output,EventEmitter } from '@angular/core';
import { InventorySearch } from '../../../models/inventory/search';
import { Router } from  '@angular/router';

@Component({
  selector:"inventory-search",
  templateUrl : './search.component.html',
  styleUrls :['search.component.scss']
})

export class SearchInventoryComponent implements OnInit{
  all_tags:Array<any>=["None"];
  searchObject:any = {};
  types:Array<string>=["Frames","Sun Glass","Contact Lens","Lens","Accessories","Extras Lens"]
  object:any = {};
  searchListConfig:any = InventorySearch.searchListConfig();
  popConfig = InventorySearch.popConfig;
  priceRange:any;
  @Input('config') config = undefined;
  @Output('inventory_clicked') inventory_clicked = new EventEmitter();
constructor(private router:Router){}


  ngOnInit(){
    if(this.config){
      this.searchListConfig["deleteUrl"] = undefined;
    }else{
      this.searchListConfig = InventorySearch.searchListConfig();
    }
  }

  tagRemoved(item){
    if(this.all_tags.length == 0){
      this.all_tags.push("None");
      this.searchObject = {};
    }else if(item.value == "tags" || item.value == "types"){
      this.onItemRemove();
    }
  }

  search(e){
    e.preventDefault();
    this.searchObject = {item_code : this.object.item_code}
    this.all_tags= ["Item Code :"+this.object.item_code];
  }

  inventoryClicked(inventory){
    if(this.config)
      this.inventory_clicked.emit(inventory);
    else
    this.router.navigate([`inventory/${inventory.type}/${inventory._id}`]);
  }

  submited(data){
    let tags = [];
    if(data.tags)
      tags = data.tags.map(tag =>{ return tag.value });
    let inventories = [];
    data.inventoyType.forEach(( item , i )=>{
      if(item)
        inventories.push(i.toString());
    });
    if(tags.length ==0 && inventories.length == 0){
      this.searchObject = {
        priceRange : data.priceRange
      };
      this.all_tags = ["None"];
    }else{
      this.searchObject={
        tags:tags,
        inventories:inventories,
        priceRange : data.priceRange
      }
      this.all_tags = [];
      tags.forEach(tag=>{
        this.all_tags.push({display:tag,value:"tags"});
      });
        inventories.forEach(inventory=>{
        this.all_tags.push({display:this.types[parseInt(inventory)],value:"types"});
      });
    }
    if(data.priceRange){
      if(this.all_tags[0] == "None") this.all_tags.pop();
      this.priceRange = data.priceRange;
      this.all_tags.push({display:`Price Range From ${data.priceRange[0]} To ${data.priceRange[1]}`,value:"price"});
    }
  }

  onItemRemove(){
    let tags = [];
    let inventories = [];
    this.all_tags.map(tag=>{
      if(tag.value == "tags"){
        tags.push(tag.display);
      }else if(tag.value == "types"){
        inventories.push(this.getTypeIndex(tag.display));
      }
    });
    this.searchObject={tags:tags,inventories:inventories,priceRange : this.priceRange};
  }

  getTypeIndex(type){
    let index = 0 ;
    this.types.forEach((t,i)=>{
      if(t == type)
        index = i;
    });
    return index.toString();
  }

  search_enter(event){
    if(event.keyCode == 13){
      this.search(event);
    }
  }
}
