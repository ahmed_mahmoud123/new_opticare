import { NgModule } from '@angular/core';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { NewInventoryComponent } from './new/new.component';
import { CartComponent } from './cart/cart.component';

import { InventoryDetailsComponent } from './details/details.component';

import { SearchInventoryComponent } from './search/search.component';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!

import { DynamicFormModule } from '../../factories/form-factory/dynamic-form.module';
import { DynamicListModule } from '../../factories/list-factory/dynamic-list.module';

@NgModule({
  declarations:[
    NewInventoryComponent,
    SearchInventoryComponent,
    InventoryDetailsComponent,
    CartComponent
  ],
  imports:[
    ReactiveFormsModule,
    FormsModule,
    DynamicFormModule,
    DynamicListModule,
    TagInputModule,
    BrowserAnimationsModule
  ],
  exports:[
    CartComponent
  ],
  entryComponents:[
    CartComponent
  ]
})

export class InventoryModule {}
