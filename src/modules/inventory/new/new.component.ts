import { Component,Inject,forwardRef  } from '@angular/core';
import { ElementFactory } from '../../../factories/element-factory/element-factory';
import { FrameInventory } from '../../../models/inventory/frame';
import { BasicInventory } from '../../../models/inventory/basicInventory';
import { AppComponent } from '../../../app/app.component';

import { CLInventory } from '../../../models/inventory/contact_lens';
import { LensInventory } from '../../../models/inventory/lens';
import { AccessoriesInventory } from '../../../models/inventory/accessories';
import { LensExtras } from '../../../models/inventory/lens_extras';

declare var $ : any;
import { FormControl } from '@angular/forms';

@Component({
  templateUrl : './new.component.html',
  styleUrls :['new.component.scss']
})

export class NewInventoryComponent{

  config:any;
  inventory:BasicInventory=   new FrameInventory();
  inventoryConfig:any = this.inventory.config;
  group:any;
  inventory_type:string="0";
  factory:any;
  app:AppComponent;
  constructor(@Inject(forwardRef(() => AppComponent)) appComponent: AppComponent){
    this.app = appComponent;
    this.config = {
      props:{id:"inventory_type"},
      "accordions":[
        {
          "title":"Item Type",
          "rows":[
            [
              ElementFactory.generateRadio("inventoyType","",12,[{text:"Frames",value:"0"},{text:"Sun Glass",value:"1"},{text:"Contact Lens",value:"2"},{text:"Lens",value:"3"},{text:"Accessories",value:"4"},{text:"Lens Extra",value:"5"}],undefined,"0")
            ]
          ]
        }
      ]
    }
  }



  groupCreated(group){
    this.group = group;
    if(!this.group.controls['type']){
      this.group.addControl("type",new FormControl(this.inventory_type));
    }
    this.inventory.getItemCode(this.group);
  }



  formChange(model){
    this.inventory_type = model.group.value.inventoyType
    switch(this.inventory_type){
      case "0":
      case "1":
        let inventory =  new FrameInventory();
        inventory.setConfig(this.inventory_type);
        this.inventory = inventory;
      break;
      case "2":
        this.inventory =  new CLInventory();
      break;
      case "3":
        this.inventory =  new LensInventory();
      break;
      case "4":
        this.inventory =  new AccessoriesInventory();
      break;
      case "5":
        this.inventory =  new LensExtras();
      break;
    }
    this.inventoryConfig = this.inventory.config;

    //this.app.resetTabIndex();
  }

  afterSubmited(){
    this.inventory.getItemCode(this.group);
  }


}
