import { Component,OnInit } from '@angular/core';
import { Cart } from '../../../models/cart/cart';
import { FormControl,FormBuilder } from '@angular/forms';
declare var $:any;
@Component({
  templateUrl : './cart.component.html',
  styleUrls :['cart.component.scss']
})

export class CartComponent implements OnInit{

  config:any;
  group:any;

  searchInventoryConfig:any= {}
  disc:number = 0;
  cart_items:Array<any> = [];
  cartModel = new Cart();
  usefulInfo:any = {price:0,total:0,disc:0,tax:0};
  fees_config:any = this.cartModel.fees_config;
  constructor(private fb:FormBuilder){
  }

  ngOnInit(){
    this.group.addControl('cart',new FormControl(""));
    this.group.addControl('usefulInfo',new FormControl(this.usefulInfo));
    this.group.addControl('value_a',new FormControl(""));
    this.group.addControl('ed',new FormControl(""));
    this.group.addControl('r_height',new FormControl(""));
    this.group.addControl('value_b',new FormControl(""));
    this.group.addControl('dbl',new FormControl(""));
    this.group.addControl('le_height',new FormControl(""));
    this.group.addControl('lens_diameter',new FormControl(""));
    this.group.addControl('re_lens_edge',new FormControl(""));
    this.group.addControl('le_lens_edge',new FormControl(""));
    this.group.addControl("precal_checkboxes",this.fb.array([new FormControl(false),new FormControl(false)]));
  }


  inventory_clicked(inv){
    if(this.group.controls['cart'].value == '-' || this.group.controls['cart'].value == "")
      this.group.controls['cart'].setValue([]);
    let inventory = Object.create(inv);
    inventory.selling_price=parseInt(inventory.selling_price+"".replace(/,/g, ''))
    switch(inventory.type){
      case "0":
        inventory.type = "Frames"
      break;
      case "1":
        inventory.type = "Sun Glass"
      break;
      case "3":
        inventory.type = "Lens"
      break;
      case "2":
        inventory.type = "Contact Lens"
      break;
      case "4":
        inventory.type = "Accessories"
      break;
    }
    this.group.controls['cart'].value.forEach(item=>{
      if(item.item_code == inventory.item_code){
        inventory.qnty = ++item.qnty;
        item.total = parseInt(item.selling_price) * item.qnty;
        item.tax = item.total * .16;
      }
    });
    inventory.qnty = inventory.qnty?inventory.qnty:1;

    if(inventory.qnty == 1){
      inventory.disc = "0.00";
      inventory.realPrice =  parseInt(inventory.selling_price);
      inventory.total = parseInt(inventory.selling_price);
      inventory.selling_price =(inventory.selling_price /  1.16).toFixed(2);
      inventory.item_code = inv.item_code;
      inventory.realTax = inventory.total - inventory.selling_price;

      inventory.tax = inventory.total - inventory.selling_price;
      inventory.desc = this.setItemDecription(inventory);
      this.group.controls['cart'].value.push(inventory);
    }
    this.calcInfo();
    this.showNotification();
  }

  deleteItem(inventory){
    let items = [];
    this.group.controls['cart'].value.forEach((item,i)=>{
      if(item.item_code != inventory.item_code)
        items.push(item);
    });
    this.group.controls['cart'].setValue(items);
    this.calcInfo();
    this.showNotification("Item is removed from cart");
  }

  add_fees(data){
    if(this.group.controls['cart'].value == '-' || this.group.controls['cart'].value == "")
      this.group.controls['cart'].setValue([]);
    this.group.controls['cart'].value.push({
      type:data.e_type.replace(/_/g, ' ').toUpperCase()+" , "+data.e_label+" - FEE",
      item_code:"fees",
      realPrice :parseInt(data.cost),
      selling_price:(parseInt(data.cost) / 1.16).toFixed(2),
      total:parseInt(data.cost),
      tax:(parseInt(data.cost)-(parseInt(data.cost) / 1.16)).toFixed(2),
      realTax:(parseInt(data.cost)-(parseInt(data.cost) / 1.16)).toFixed(2),
      qnty:1,
      disc:"0.00"
    });
    this.calcInfo();
    this.showNotification();
  }

  showNotification(msg = "Item is added to cart"){
    $.bootstrapGrowl(msg, {type: 'success'});
  }

  examTypeChange(data){
    this.cartModel.changeValues(data.group.value.e_type);
  }

  discount(value,index){
    value = value == ""?0:value;
    let item = this.group.controls['cart'].value[index];
    item.total = (item.realPrice * item.qnty) - value;
    item.tax = item.realTax * item.qnty;
    item.disc = value;
    this.calcInfo();
  }

  qnty(value,index){
    value = value==""?1:value;
    let item = this.group.controls['cart'].value[index];
    item.qnty = value;
    item.total = (item.realPrice * item.qnty)  - Number(item.disc);
    item.tax = (item.realTax * item.qnty);
    this.calcInfo();
  }

  calcInfo(){
    this.usefulInfo = {price:0,total:0,disc:0,tax:0};
    this.group.controls['cart'].value.forEach(item=>{
      this.usefulInfo.price += item.selling_price;
      this.usefulInfo.total += item.total;
      this.usefulInfo.disc += parseInt(item.disc);
      this.usefulInfo.tax += item.tax;
    });
    this.group.controls['usefulInfo'].setValue(this.usefulInfo);
  }


  setItemDecription(doc){
    let desc = "";
    if(doc.brand && doc.brand != '-')
      desc+=" "+doc.brand;
    if(doc.size && doc.size != '-')
      desc+=" "+doc.size;
    if(doc.model_name && doc.model_name != '-')
      desc+=" "+doc.model_name;
    if(doc.model_number && doc.model_number != '-')
      desc+=" "+doc.model_number;
    if(doc.model_number && doc.model_number != '-')
      desc+=" "+doc.model_number;
    if(doc.color_Code && doc.color_Code != '-')
      desc+=" "+doc.color_Code;
    if(doc.modality && doc.modality != '-')
      desc+=" "+doc.modality;
    if(doc.lens_type && doc.lens_type != '-')
      desc+=" "+doc.lens_type;
    if(doc.index && doc.index != '-')
      desc+=" "+doc.index;
    return desc;
  }
  ngAfterViewInit(){
    /*setTimeout(()=>{
      $('#cart_tbl').DataTable({
          "order": [[ 1, "desc" ]],
          searching: false,
          paging: false,
          bFilter: false,
          bInfo: false
      });
    },1000)*/
  }
}
