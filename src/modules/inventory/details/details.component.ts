import { Component  ,OnInit } from '@angular/core';
import { FrameInventory } from '../../../models/inventory/frame';
import { CLInventory } from '../../../models/inventory/contact_lens';
import { LensInventory } from '../../../models/inventory/lens';
import { AccessoriesInventory } from '../../../models/inventory/accessories';
import { LensExtras } from '../../../models/inventory/lens_extras';

import { ActivatedRoute } from '@angular/router';

declare var $ : any;

@Component({
  templateUrl : './details.component.html',
  styleUrls :['details.component.scss']
})

export class InventoryDetailsComponent implements OnInit{

  inventoryConfig:any;
  constructor(private activatedRoute:ActivatedRoute){}

  ngOnInit(){
    switch(this.activatedRoute.snapshot.params['type']){
      case "0":
      case "1":
        this.inventoryConfig = FrameInventory.getInstance().getEditConfig(this.activatedRoute.snapshot.params['type']);
      break;
      case "2":
        this.inventoryConfig = CLInventory.getInstance().getEditConfig();
      break;
      case "3":
        this.inventoryConfig = LensInventory.getInstance().getEditConfig();
      break;
      case "4":
        this.inventoryConfig = new AccessoriesInventory().getEditConfig();
        break;
      case "5":
        this.inventoryConfig = LensExtras.getInstance().getEditConfig();
        break;
    }
  }
  ngDoCheck(){
    $('#item_code').prop('disabled',true);
  }


}
