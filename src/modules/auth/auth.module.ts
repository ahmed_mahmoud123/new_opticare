import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerModule } from 'angular2-spinner/dist';

import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations:[
    LoginComponent
  ],
  imports:[
    RouterModule,
    ReactiveFormsModule,
    CommonModule,
    SpinnerModule
  ]
})

export class AuthModule {}
