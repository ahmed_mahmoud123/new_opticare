import { Component } from '@angular/core';
import { FormBuilder , FormGroup , Validators } from '@angular/forms';
import { GenericService } from '../../../providers/generic.service';

@Component({
  templateUrl : './login.component.html',
  styleUrls :['login.component.scss']
})

export class LoginComponent {

  loginForm:FormGroup;
  error:boolean = false;
  working:boolean = false;
  constructor(private __genericService:GenericService,private fb:FormBuilder){
    document.getElementsByTagName('body')[0].style.backgroundColor = "#364150";
    this.loginForm = this.fb.group({
      name:['',Validators.required],
      pass:['',Validators.required]
    });
  }

  login(){
    this.error = false;
    if(this.loginForm.valid){
      this.working = true;
      this.__genericService.post("auth/login",{
        username:this.loginForm.value.name,
        password : this.loginForm.value.pass
      }).then(result=>{
        this.working = false;
        localStorage.setItem('api_token', result.token);
        window.location.href = "/patient";
      },err=>{
        this.working = false;
        this.error = true;
      }).catch(err=>this.working = false);
    }


  }


}
