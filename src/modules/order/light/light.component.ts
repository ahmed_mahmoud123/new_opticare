import { Component,OnInit,Input  } from '@angular/core';
import { Order } from '../../../models/order/order';
import { ActivatedRoute,Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { PrintService } from '../../../providers/print.service';

declare var $:any;
@Component({
  selector:"order",
  templateUrl : './light.component.html',
  styleUrls :['light.component.scss'],
  providers:[PrintService]
})

export class LightOrderComponent implements OnInit{
  config:any;
  order:any;
  group:any;
  patient_id:any;
  p_type:string;
  exam_id:any;
  total:any;
  printConfig:any = {type:0};
  @Input('isEdit') isEdit:boolean =false;
  constructor(private router:Router,protected activatedRoute:ActivatedRoute,protected printService:PrintService){}

  ngOnInit(){
    this.order = new Order(this.print_detaied_order,this.print_summary_order,this);
    this.exam_id = this.activatedRoute.snapshot.params['exam_id'];
    if(this.isEdit){
      this.config = this.order.getEditConfig(this.activatedRoute.snapshot.params['id']);
      this.order.add_print_btns();
    }else{
      this.patient_id = this.activatedRoute.snapshot.params['id'];
      this.config = this.order.config;
    }
  }



  groupCreated(group){
    this.group = group;
    this.group.addControl("payment_summary",new FormControl([]));
    this.group.valueChanges.subscribe(val => {
      if(val.usefulInfo && val.usefulInfo.total != this.total){
        this.total = val.usefulInfo.total;
        this.group.controls['total_amount'].setValue((val.usefulInfo.total).toLocaleString('en-IN'));
        if(val.amount_paid){
          this.group.controls['balance_amount'].setValue((parseInt(val.usefulInfo.total) - parseInt(val.amount_paid+"".replace(/,/g, ''))).toLocaleString('en-IN'));
        }
      }
      if(val.p_type && val.p_type !="" && this.p_type != val.p_type ){
        this.p_type = val.p_type;
        if(val.p_type == "private"){
          this.config.accordions[4].rows[3].pop();
        }else{
          this.config.accordions[4].rows[3].push(this.order.insurance_status);
        }
      }
    });
    if(!this.isEdit){
      this.order.getOrderNumber(group);
      this.order.poulatePatientData(this.patient_id,group);

    }
  }

  formChange(data){
    let group = data.group;
    group.controls["delivered_date"].setValue(this.order.getDeliveryDate(group.value.order_date));
  }

  afterGroupPopulate(model){
    if(this.isEdit){
      this.afterSubmitBillInfoUpdate(model);
    }
  }

  afterSubmited(event){

    let p_s = this.group.controls["payment_summary"].value;
    let amount_paid = 0 ;
    if(p_s){
      if(event.payment1!="-"){
        p_s.push({p_date:event.order_date,p_method:event.payment1,p_amount:event.deposit_amount1,comment:event.comment_method_1})
        amount_paid+=parseInt(event.deposit_amount1);
      }if(event.payment2!="-"){
        p_s.push({p_date:event.order_date,p_method:event.payment2,p_amount:event.deposit_amount2,comment:event.comment_method_2})
        amount_paid+=parseInt(event.deposit_amount2);
      }
    }
    if(this.isEdit)
      this.group.controls["amount_paid"].setValue(parseInt(this.group.controls["amount_paid"].value)+amount_paid);
    else
      this.group.controls["amount_paid"].setValue(amount_paid);
    this.group.controls["payment_summary"].setValue(p_s);
    this.afterSubmitBillInfoUpdate(event);
    if(this.order.btns_bar.buttons.length<2)
      this.order.add_print_btns();
    if(!this.isEdit)
      this.order.getOrderNumber(this.group);
    if(event.order_number){
      //this.router.navigate(["/order/"+event.order_number]);
      this.router.navigate(["/order/search"]);
    }
  }

  print_detaied_order(){
    this.printConfig.type = 0;
    console.log(this.group.value);
    setTimeout(()=>PrintService.print("order","portrait"),50);
  }
  print_summary_order(){
    this.printConfig.type = 1;
    setTimeout(()=>PrintService.print("order","portrait"),50);
  }

  ngAfterViewInit(){
    if(this.exam_id){
      this.group.controls['exam_id'].patchValue(this.exam_id);
    }
    document.getElementById("total_amount").setAttribute("readonly", "true");
    document.getElementById("amount_paid").setAttribute("readonly", "true");
    document.getElementById("balance_amount").setAttribute("readonly", "true");
  }

  afterSubmitBillInfoUpdate(model){
    this.group.controls["deposit_amount1"].setValue("");
    this.group.controls["deposit_amount2"].setValue("");
    this.group.controls["comment_method_1"].setValue("");
    this.group.controls["comment_method_2"].setValue("");
    this.group.controls["payment1"].setValue("-");
    this.group.controls["payment2"].setValue("-");
    $('.select2').trigger('change.select2');
    this.group.controls["amount_paid"].setValue(parseInt(model.amount_paid).toLocaleString('en-IN'));
  }
}
