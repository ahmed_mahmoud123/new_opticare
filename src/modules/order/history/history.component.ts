import { Component,OnInit  } from '@angular/core';
import { GenericService } from '../../../providers/generic.service';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormControl} from '@angular/forms';

@Component({
  selector:'order-history',
  templateUrl : './history.component.html',
  styleUrls :['./history.component.scss']
})

export class OrderHistoryComponent{
  patient:any = {};
  patientOrders:any;
  constructor(private title:Title,private __genericService:GenericService,private activatedRoute:ActivatedRoute){
    let id = this.activatedRoute.snapshot.params['id'];
    this.patientOrders = {
      dataSource:"order/patient/"+id,
      colsNames:["Order Number","Order Date","Order Status","Insurance Status","Delivered Date"],
      fieldsNames:["order_number","order_date","order_status","insurance_status","delivered_date"],
    }
    __genericService.get("patient/details/"+id).then(data=>{
      if(data){
        this.patient = data;
        this.title.setTitle(`Orders History - ${data.first_name} ${ data.last_name}`)
      }
    });
    __genericService.get("exam/get_patient_exams/"+id).then(data=>{

    });
  }

}
