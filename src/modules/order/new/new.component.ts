import { Component  } from '@angular/core';
import { Router } from  '@angular/router';


@Component({
  templateUrl : './new.component.html',
  styleUrls :['new.component.scss']
})

export class NewOrderComponent{


  constructor(private router:Router){}

  patientRowClicked(patient){
    this.router.navigate([`/order/${patient}`]);
  }
}
