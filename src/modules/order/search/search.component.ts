import { Component,Input,Output,EventEmitter,OnInit  } from '@angular/core';
import { SearchOrder } from '../../../models/order/search';
import { Order } from '../../../models/order/order';
import { Router } from '@angular/router'
declare var $:any;
@Component({
  selector:"order-search",
  templateUrl : './search.component.html',
  styleUrls :['./search.component.scss']
})

export class OrderSearchComponent implements OnInit{

  @Input('title') title :string = "Orders List";
  @Input('patientOrders') patientOrders :any;
  searchOrder: SearchOrder = new SearchOrder();
  @Input() defaultClick:boolean = true;
  @Output() orderRowClicked:any = new EventEmitter();
  order_status_popConfig = this.searchOrder.getOrderStatusPopConfig();
  insurance_status_popConfig =this.searchOrder.getInsuranceStatusPopConfig();
  searchGroup:any;
  searchListConfig:any;
  searFormConfig:any = this.searchOrder.getSearchFormConfig(this.show_order_status_popup,this.show_insurance_status_popup);
  searchObject:any = {};
  orderConfig = new Order();
  constructor(private __router:Router){}

  rowClicked(row){
    this.__router.navigate(['order/edit/'+row.order_number]);
  }
  ngOnInit(){
    let config = this.searchOrder.getlistConfig(this.formatData);
    if (this.patientOrders){
      config.dataSource = this.patientOrders.dataSource;
      config.colsNames = this.patientOrders.colsNames;
      config.fieldsNames = this.patientOrders.fieldsNames;
    }
    this.searchListConfig = config;
  }
  searchGroupCreated(group){
    this.searchGroup = group;
  }
  search(data){
    console.log(data);
    let tags = [];
    if(data.order_tags){
       tags = data.order_tags.map(tag =>{ return tag.value });
       this.searchObject= { tags:tags } ;
    }else{
      this.searchObject= {} ;
    }
  }

  show_order_status_popup(){
    $("#modal_order_status_popup").modal("show");
  }

  show_insurance_status_popup(){
    $("#modal_insurance_status_popup").modal();
  }

  order_status_filter(data){
    if(data.order_status)
      this.filterStatus(data.order_status,0);
  }

  insurance_status_filter(data){
    if(data.insurance_status)
      this.filterStatus(data.insurance_status,1);
  }

  filterStatus(data,type){
    let tags = [];
    for (let i = 0; i < data.length; i++) {
        if(data[i]){
          if(type == 0){
            tags.push(this.orderConfig.order_status_options[i]);
          }else if(type == 1){
            tags.push(this.orderConfig.insurance_status_options[i]);
          }
        }
    }
    this.searchObject = { tags:tags } ;
    let optionsTags = tags.map(tag=>{
      return {display : tag , value:tag }
    })
    this.searchGroup.controls['order_tags'].setValue(optionsTags);
  }

  formatData(row){
    if(row.order_status == "COLLECTED")
      row.delivered_date = "-";
  }

}
