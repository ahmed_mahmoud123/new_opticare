import { Component,Input} from '@angular/core';
import { GenericService } from '../../../providers/generic.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector:'receipt',
  templateUrl : './receipt.component.html',
  styleUrls :['./receipt.component.scss']
})

export class ReceiptComponent{

  order:any = {};
  desposit_paid:number= 0;
  balanceToPay:number= 0;
  methods:Array<any>= [];
  receipt_id:number;
  receipt_date:any;
  constructor(private activatedRoute:ActivatedRoute,private genericService:GenericService){
  }

  ngOnInit(){
    let order_id = this.activatedRoute.snapshot.params['order_id'];
    this.receipt_id = this.activatedRoute.snapshot.params['receipt_id'];

    this.genericService.get("order/get/"+order_id).then(result=>{
      this.order = result;
      for (let i = 0; i < this.order.payment_summary.length; i++) {
          if(this.order.payment_summary[i].receipt == this.receipt_id){
            this.methods.push(this.order.payment_summary[i]);
            this.receipt_date = this.order.payment_summary[i].p_date;
            this.balanceToPay = this.order.payment_summary[i].balance_amount;
            this.desposit_paid+= Number(this.order.payment_summary[i].p_amount);
          }
      }
    });
  }


}
