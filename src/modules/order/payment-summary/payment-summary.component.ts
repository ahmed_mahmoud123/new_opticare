import { Component,Input} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector:'payment_summary',
  templateUrl : './payment-summary.component.html',
  styleUrls :['./payment-summary.component.scss']
})

export class PaymentSummaryComponent{

  config:any;
  group:any;
  @Input() printGroup;
  constructor(private router:Router){}

  ngOnInit(){
    if(this.printGroup){
      this.group = this.printGroup;
    }
  }

  getRecipet(evt,order_number,receipt){
    evt.preventDefault();
    console.log(receipt);
    this.router.navigate(['/receipt/'+order_number+"/"+receipt]);
  }
}
