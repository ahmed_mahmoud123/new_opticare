import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NewOrderComponent } from './new/new.component';
import { LightOrderComponent } from './light/light.component';
import { OrderSearchComponent } from './search/search.component';
import { OrderHistoryComponent } from './history/history.component';
import { CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import { DynamicFormModule } from '../../factories/form-factory/dynamic-form.module';
import { DynamicListModule } from '../../factories/list-factory/dynamic-list.module';
import { PatientModule } from '../patient/patient.module';
import { OrderEditComponent } from './edit/edit.component';
import { PaymentSummaryComponent } from './payment-summary/payment-summary.component';
import { PrintOutDetailedComponent } from './printout-detailed/printout-detailed.component';
import { ReceiptComponent } from './reciept/receipt.component';

@NgModule({
  declarations:[
    NewOrderComponent,
    LightOrderComponent,
    OrderSearchComponent,
    OrderHistoryComponent,
    OrderEditComponent,
    PaymentSummaryComponent,
    PrintOutDetailedComponent,
    ReceiptComponent
  ],
  imports:[
    FormsModule,
    DynamicFormModule,
    DynamicListModule,
    PatientModule,
    CommonModule,
    RouterModule
  ],
  exports:[
    OrderSearchComponent,
    OrderHistoryComponent,
    PaymentSummaryComponent
  ],
  entryComponents:[
    PaymentSummaryComponent
  ]
})

export class OrderModule {}
