import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerModule } from 'angular2-spinner/dist';

import { CashBookComponent } from './cashbook/cashbook.component';
import { RefundComponent } from './refund/refund.component';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations:[
    CashBookComponent,
    RefundComponent
  ],
  imports:[
    RouterModule,
    CommonModule,
    SpinnerModule,
    FormsModule
  ]
})

export class FinanceModule {}
