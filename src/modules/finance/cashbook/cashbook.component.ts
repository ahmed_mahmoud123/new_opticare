import { Component } from '@angular/core';
import { GenericService } from '../../../providers/generic.service';
declare var $:any;
@Component({
  templateUrl : './cashbook.component.html',
  styleUrls :['cashbook.component.scss']
})

export class CashBookComponent {

  date:any;
  transactions:Array<any> = [];
  refunds:Array<any> = [];
  cashesInfo :any = {};
  refundAmount:any = 0;
  tentativeClosingCash:number = 0;
  bnk_deposite:number = 0;
  actual_cache_in_hand:number	= 0;
  discrepancy:number = 0;
  sales:number = 0;
  petty_cash:number = 0;
  constructor(private __genericService:GenericService){
    let date = new Date();
    this.date = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
    this.getTransactions(this.date);
    this.getRefunds();
    this.getOpeningCashInfo(this.date);
  }

  ngAfterViewInit(){
    let self = this;
    $("#datepicker").datepicker()
    .on('changeDate',function(e){
      self.date = `${e.date.getDate()}/${e.date.getMonth()+1}/${e.date.getFullYear()}`;
      self.getTransactions(self.date);
      self.getRefunds();
      self.getOpeningCashInfo(self.date);
    });
  }


  getTransactions(date){
    this.__genericService.post("order/getTransactions",{date:this.date}).then(result=>{
      this.transactions = result;
    });
  }

  getRefunds(){
    this.__genericService.post("refund/get",{date:this.date}).then(result=>{
      this.refunds = result
    });
  }

  getOpeningCashInfo(date){
    this.__genericService.post("cashes/get",{date:date}).then(result=>{
      this.cashesInfo = result;
      if(this.cashesInfo.date == date ){
        this.cashesInfo.openingCash = this.cashesInfo.openingCash?this.cashesInfo.openingCash:0;
        this.sales = this.cashesInfo.sales?this.cashesInfo.sales:0
        this.petty_cash = this.cashesInfo.petty_cash?this.cashesInfo.petty_cash:0
        this.discrepancy = this.cashesInfo.discrepancy?this.cashesInfo.discrepancy:0
        this.tentativeClosingCash = this.cashesInfo.tentativeClosingCash?this.cashesInfo.tentativeClosingCash:0
        this.bnk_deposite = this.cashesInfo.bnk_deposite?this.cashesInfo.bnk_deposite:0;
        this.refundAmount = this.cashesInfo.refundAmount?this.cashesInfo.refundAmount:0;
        this.actual_cache_in_hand = this.cashesInfo.actual_cache_in_hand?this.cashesInfo.actual_cache_in_hand:0;
      }else if(!this.cashesInfo.date){
        this.cashesInfo.openingCash = this.cashesInfo.openingCash?this.cashesInfo.openingCash:0;
        this.sales = this.petty_cash = this.discrepancy = this.bnk_deposite = this.tentativeClosingCash = this.refundAmount = this.actual_cache_in_hand =0;
      }else{
        this.cashesInfo.openingCash = this.cashesInfo.actual_cache_in_hand?this.cashesInfo.actual_cache_in_hand:0;
        this.sales = this.petty_cash = this.discrepancy = this.bnk_deposite = this.tentativeClosingCash = this.refundAmount = this.actual_cache_in_hand =0;
      }
      this.saveCashDay();
      //this.getRefundAmount();
    });
  }

  getRefundAmount(){
    this.__genericService.post("refund/getRefundAmount",{date:this.date}).then(result=>{
      this.refundAmount = result.pop();
      this.refundAmount = this.refundAmount?this.refundAmount.sum : 0;
      this.tentativeClosingCash = (this.cashesInfo.openingCash - this.refundAmount) + Number(this.bnk_deposite);
      this.discrepancy = this.tentativeClosingCash - this.actual_cache_in_hand;
      this.saveCashDay();
    });
  }

  bank_deposite(value){
    if(!isNaN(value)){
      this.bnk_deposite = value;
      this.tentativeClosingCash = (this.cashesInfo.openingCash - this.refundAmount) + Number(this.bnk_deposite);
      this.discrepancy = this.tentativeClosingCash - this.actual_cache_in_hand;
      this.saveCashDay();
    }
  }

  actual_in_hand(value){
    if(!isNaN(value)){
      this.actual_cache_in_hand = Number(value);
      this.discrepancy = this.tentativeClosingCash - this.actual_cache_in_hand;
      this.saveCashDay();
    }
  }

  afterRefundSave(refundObj){
    this.refunds.push(refundObj);
    this.getRefundAmount();
  }

  saveCashDay(){
    this.__genericService.post("cashes/new",
    {
      date:this.date,
      sales:this.sales,
      openingCash : this.cashesInfo.openingCash,
      petty_cash : this.petty_cash,
      tentativeClosingCash:this.tentativeClosingCash,
      bnk_deposite : this.bnk_deposite,
      actual_cache_in_hand :this.actual_cache_in_hand ,
      discrepancy : this.discrepancy,
      refundAmount:this.refundAmount
    }).then(result=>{
      console.log(result)
    });
  }
}
