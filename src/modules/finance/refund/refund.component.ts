import { Component,EventEmitter,Output } from '@angular/core';
import { GenericService } from '../../../providers/generic.service';
import { Order } from '../../../models/order/order';
declare var $:any;
@Component({
  selector:"app-refundPopup",
  templateUrl : './refund.component.html',
  styleUrls :['refund.component.scss']
})

export class RefundComponent {
  orderModel:Order= new Order();
  refundObj:any = {};
  @Output() afterSave = new EventEmitter();
  date:any;
  constructor(private __genericService:GenericService){
    let date = new Date();
    this.date = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
    this.orderModel.payment_methods.shift();
  }


  getRecipetDetails(e){
    this.refundObj.reciept_no = e.target.value;
    this.__genericService.post("order/getRecipetDetails",{receipt:e.target.value}).then(result=>{
      if(result && result.length){
        this.refundObj.amount = 0 ;
        for (let i = 0; i < result.length; i++) {
            this.refundObj.amount +=Number(result[i].payment_summary.p_amount);
        }
        let receipt = result.pop();
        this.refundObj.patient_name = receipt.patient_id.first_name+" "+receipt.patient_id.last_name;
        this.refundObj.order_no = receipt.order_number;
      }else{
        this.refundObj = {};
      }
    });
  }

  save(){
    this.refundObj.date = this.date;
    this.__genericService.post('refund/new',this.refundObj).then(result=>{
      this.afterSave.emit(Object.assign({},this.refundObj));
      $('#refund_popup').modal('hide');
    });
  }

}
