import { Component , Input ,OnInit,OnChanges} from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { CustomValidator } from '../../validators/customValidator';
import { FormGroup,Validators,FormControl } from '@angular/forms';
import { ServiceLocator } from '../../providers/service-locator';

@Component({
  template:''
})

export class BasicExamComponent implements OnChanges,OnInit{
  protected examModel:any;
  protected child:any;

  config:any;
  @Input() accordionsConfig = undefined;
  group:any;
  @Input() examConfig;
  @Input() patient;
  router:Router;
  patient_data;
  constructor(protected activatedRoute:ActivatedRoute){


  }
  ngOnChanges(changes: any){
    if(changes.examConfig && changes.examConfig.currentValue){
        this.examModel = new this.child();
        this.config = this.examModel.generateEditConfig(changes.examConfig.currentValue.id);
        this.examModel.setLabel(changes.examConfig.currentValue.label);
        this.orderConfig(this.config.props);
      }
    if(changes.patient){
      this.examModel.setPatient(changes.patient.currentValue);
    }
  }

  ngOnInit(){

    if(!this.examConfig){
      this.examModel = new this.child();
      this.config = this.examModel.config;
    }
    // For Order Page purpose ...
    this.orderConfig(this.config.props);
  }

  orderConfig(props){
    if(this.accordionsConfig){
      let config ={props:props,accordions:[]};
      this.config.accordions.forEach((acc,i)=>{
        if(this.in_array(this.accordionsConfig,acc.title)){
          config.accordions.push(this.config.accordions[i]);
        }
      });
      this.config = config;
    }
  }
  groupCreated(group){
    this.group = group;
    if(!this.activatedRoute.snapshot.data['order']){
      let params =this.activatedRoute.snapshot.params;
      this.examModel.getPatientData(params,group).then(data=>{
        this.patient_data = data;
      });
    }


  }

  afterSubmited($event){

    if($event.source == "save_exam_order"){
      let p_id = this.activatedRoute.snapshot.params['id'];
      this.router = ServiceLocator.injector.get(Router);
      this.router.navigate(['/order/'+p_id]);
    }
  }

  in_array(array:Array<any>,value:string):boolean{
    for (let i = 0; i < array.length; i++) {
      if(array[i] == value)
        return true;
    }
    return false;
  }

  // date come from the server when update
  modelData(data){

    this.examModel.setOrderFn(()=>{
      let router = ServiceLocator.injector.get(Router);
      router.navigate(['/order_exam/'+this.patient._id+'/'+this.examConfig.id]);
    },self);

  }

  setValidationRuleForAxis(data){
    let group = data.group;

    if(this.group.controls['cyl'].value != '-' || this.group.controls['l_cyl'].value != '-')
    {
      this.group.controls['axis'].setValidators(CustomValidator.isTableRequired);
      this.group.controls['l_axis'].setValidators(CustomValidator.isTableRequired);
    }else{
      this.group.controls['axis'].clearValidators();
      this.group.controls['l_axis'].clearValidators();
    }
    this.group.controls['l_axis'].updateValueAndValidity();
    this.group.controls['axis'].updateValueAndValidity();
  }

}
