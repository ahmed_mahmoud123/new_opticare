import { Component,Input  } from '@angular/core';
import { PrintService } from '../../../providers/print.service';

@Component({
  selector:"printout-eye",
  templateUrl : './printout-eye.component.html',
  styleUrls :['./printout-eye.component.scss'],
  providers:[PrintService]
})

export class PrintOutEyeComponent{

  @Input('patient') patient:any;
  @Input('group') group:any;
  constructor(protected printService:PrintService){}

  print(){
    PrintService.print("eye_exam_print");
  }
}
