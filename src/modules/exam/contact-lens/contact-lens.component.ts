import { Component,Input} from '@angular/core';
import { ContactExam } from '../../..//models/contactExam';
import { BasicExamComponent } from '../basic-exam.component';
import { PrintService } from '../../../providers/print.service';

import { ActivatedRoute } from '@angular/router';
declare var $:any;

@Component({
  selector :'contact-lens',
  templateUrl : './contact-lens.component.html',
  styleUrls :['contact-lens.component.scss']
})

export class ContactLensComponent extends BasicExamComponent{

  protected child =  ContactExam;

  constructor(protected activatedRoute:ActivatedRoute){
    super(activatedRoute);
  }

  ngOnInit(){
    super.ngOnInit();
    this.examModel.setPrintfn(this.print);
  }

  print(){
    PrintService.print("contact_exam_print");
  }



  ngAfterViewInit(){
    $("#reExamination_advised").attr({disabled:true});
  }

  formChange(data){
    this.setValidationRuleForAxis(data);
  }
}
