import { Component,Input  } from '@angular/core';
import { PrintService } from '../../../providers/print.service';

@Component({
  selector:"printout-contact",
  templateUrl : './printout-contact.component.html',
  styleUrls :['./printout-contact.component.scss'],
  providers:[PrintService]
})

export class PrintOutContactComponent{

  @Input('patient') patient:any;
  @Input('group') group:any;
  constructor(protected printService:PrintService){}

  print(){
    PrintService.print("contact_exam_print");
  }
}
