import { Component  } from '@angular/core';
declare var $:any;

@Component({
  templateUrl : './new.component.html',
  styleUrls :['./new.component.scss']
})

export class NewExamComponent{

  patient_id:number;

  constructor(){}
  patientRowClicked(row){
    this.patient_id = row;
    $('#modal_exam_type').modal();
  }

}
