import { Component,OnInit  } from '@angular/core';
import { GenericService } from '../../../providers/generic.service';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { FormControl} from '@angular/forms';

@Component({
  selector:'exam-history',
  templateUrl : './history.component.html',
  styleUrls :['./history.component.scss']
})

export class ExamHistoryComponent implements OnInit{

  config:any;
  group:any;
  exams:Array<any>=[];
  examConfig:any;
  exam_id:string;
  patient:any = {};
  rows:Array<any>=[];
  accordionsConfig:any;
  constructor(private title:Title,private __genericService:GenericService,private activatedRoute:ActivatedRoute){

  }

  ngOnInit(){
    if(this.config){
      this.accordionsConfig = this.config.accordions;
      this.group.addControl("exam_id",new FormControl(""));
      this.group.addControl("examData",new FormControl(""));
    }
    if(this.config && this.config.edit){
      this.group.valueChanges.subscribe(val => {
        if(this.exam_id !=val.exam_id){
          this.exam_id =val.exam_id;
          if(this.exam_id != "" && this.exam_id != "-"){
            this.getPatientExams(val.patient_id,this.exam_id);
          }
        }
      });
    }else{
      let id = this.activatedRoute.snapshot.params['id'];
      this.exam_id = this.activatedRoute.snapshot.params['exam_id'];
      this.getPatientExams(id,this.exam_id);
    }
  }


  getPatientExams(id,exam_id=undefined){
    this.__genericService.get("patient/details/"+id).then(data=>{
      if(data){
        this.patient = data;
        if(!this.config)
          this.title.setTitle(`Examination History - ${data.first_name} ${ data.last_name}`)
      }
    });

    this.__genericService.get("exam/get_patient_exams/"+id).then(data=>{
      let active_index = 0;
      if(data.length){
        if(exam_id){
          for (let i = 0; i < data.length; i++) {
              if(data[i]._id == exam_id){
                active_index=i;
                this.examConfig = {hasPrismValue:data[i].hasPrismValue,id:data[i]._id,label:data[i].exam_label,type:data[i].exam_type};
                this.group.controls['examData'].setValue(data[i]);
              }
          }
        }else{
          this.examConfig = {hasPrismValue:data[0].hasPrismValue,id:data[0]._id,label:data[0].exam_label,type:data[0].exam_type};
          if(this.config){
            this.group.controls['exam_id'].setValue(data[0]._id);
            this.group.controls['examData'].setValue(data[0]);
          }
        }
        if(this.config)
          this.toggelPrimse(this.examConfig);
        this.exams=data.map(ex=>{
          let exam_type;
          switch(ex.exam_type){
            case "EYE_EXAM":
              exam_type = "EYE EXAM";
            break;
            case "CONTACT_EXAM":
              exam_type = "CONTACT LENS EXAM";
            break;
          }
          this.rows.push([ex.examination_date,ex.time,exam_type,ex.exam_label]);

          return {active_index:active_index,id:{examData:ex,hasPrismValue:ex.hasPrismValue,id:ex._id,label:ex.exam_label,type:ex.exam_type},text:ex.examination_date+"  "+ex.time+" "+exam_type+" "+ex.exam_label};
        });
      }
    });

  }
  exam_date_selected(data){
    this.examConfig = data.id;
    // For Order Page
    if(this.group){
      this.group.controls['exam_id'].setValue(data.id.id);
      this.group.controls['examData'].setValue(data.id.examData);
      this.toggelPrimse(this.examConfig);
    }
  }

  toggelPrimse(exam){
    if(exam && exam.hasPrismValue && this.accordionsConfig.length < 2){
      this.accordionsConfig.push("Prisms");
    }else if(!exam.hasPrismValue && this.accordionsConfig.length == 2){
       this.accordionsConfig.pop();
    }
  }
}
