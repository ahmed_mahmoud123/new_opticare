import { Component , Input ,OnInit,OnChanges} from '@angular/core';
import { BasicExamComponent } from '../basic-exam.component';
import { EyeExam } from '../../..//models/eyeExam';
declare var $ : any ;
import { ActivatedRoute } from '@angular/router';
import { PrintService } from '../../../providers/print.service';


@Component({
  selector:'eye-exam',
  templateUrl : './eye.component.html',
  styleUrls :['eye.component.scss'],
})

export class EyeComponent extends BasicExamComponent{
  protected child =  EyeExam;

  constructor(protected activatedRoute:ActivatedRoute){
    super(activatedRoute);
  }


  ngOnInit(){
    super.ngOnInit();
    this.examModel.setPrintfn(this.print);
  }

  print(){
    PrintService.print("eye_exam_print");
  }

  formChange(data){
    let group = data.group;
    this.setValidationRuleForAxis(data);
    group.controls['pd'].setValue(Number(group.controls['mono_pd'].value)+Number(group.controls['l_mono_pd'].value));
    group.controls['near_pd'].setValue(Number(group.controls['mono_npd'].value)+Number(group.controls['l_mono_npd'].value));
    group.controls['reExamination_advised'].setValue(this.examModel.setReExam(group.controls['rexam_after'].value));
  }



}
