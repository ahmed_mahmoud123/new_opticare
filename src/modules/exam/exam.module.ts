import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DynamicFormModule } from '../../factories/form-factory/dynamic-form.module';
import { DynamicListModule } from '../../factories/list-factory/dynamic-list.module';
import { ContactLensComponent } from './contact-lens/contact-lens.component';
import { EyeComponent } from './eye/eye.component';
import { BasicExamComponent } from './basic-exam.component';
import {SelectModule} from '../../components/select2/ng2-select';

import { NewExamComponent } from './new/new.component';
import { SharedModule } from '../shared/shared.module';

import { ExamHistoryComponent } from './history/history.component';
import { PrintOutEyeComponent } from './printout-eye/printout-eye.component';
import { PrintOutContactComponent } from './printout-contact/printout-contact.component';

import {Injector} from "@angular/core";
import { PatientModule } from '../patient/patient.module';

@NgModule({
  declarations:[
    ContactLensComponent,
    EyeComponent,
    NewExamComponent,
    ExamHistoryComponent,
    BasicExamComponent,
    PrintOutEyeComponent,
    PrintOutContactComponent
  ],
  imports:[
    SelectModule,
    RouterModule,
    CommonModule,
    DynamicFormModule,
    DynamicListModule,
    PatientModule,
    SharedModule
  ],
  exports:[
    ExamHistoryComponent
  ],
  entryComponents:[
    ExamHistoryComponent
  ]
})

export class ExamModule {}
