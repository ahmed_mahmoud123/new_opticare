import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormModule } from '../../factories/form-factory/dynamic-form.module';
import { DynamicListModule } from '../../factories/list-factory/dynamic-list.module';
import { SelectExamTypeComponent } from './select-exam-type/select-exam-type.component';
@NgModule({
  declarations:[
    SelectExamTypeComponent
  ],
  imports:[

    DynamicFormModule,
    DynamicListModule,
    CommonModule
  ],
  exports:[
    SelectExamTypeComponent
  ]
})

export class SharedModule {}
