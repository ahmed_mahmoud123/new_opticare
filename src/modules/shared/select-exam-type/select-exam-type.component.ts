import { Component , Input } from '@angular/core';
import { ElementFactory } from '../../../factories/element-factory/element-factory';
import { Router } from  '@angular/router';
declare var $:any;

@Component({
  selector:"exam-type",
  templateUrl : './select-exam-type.component.html'
})

export class SelectExamTypeComponent{
  eye_exam_optios = [{text:"STANDARD",value:"STANDARD"},{text:"RECHECK",value:"RECHECK"}];
  contact_exam_optios = [{text:"CHECKUP",value:"CHECKUP"},{text:"TEACH",value:"TEACH"},{text:"FITTING",value:"FITTING"}];
  group:any;
  @Input() patient_id:number;
  config:any={
    "props":{
      id:"popup_search"
    },
    "accordions":[
      {
        title:"Exam Details",
        rows:[
          [
            ElementFactory.generateSelect("e_type","Exam Type",5,[{text:"Eye Exam",value:"eye_exam"},{text:"Contact Lens Exam",value:"contact_exam"}],undefined,"eye_exam"),
            ElementFactory.generateSelect("e_label","Exam Label",5,this.eye_exam_optios,undefined,"STANDARD")
          ],
          [
            ElementFactory.generateButton([{id:"select",text:"Select"}])
          ]
        ]
      }
    ]
  }

  
  constructor(private router:Router){

  }

  submited(event){
    $('#modal_exam_type').modal('hide');
    let examType:string="";
    switch(event.e_type){
      case 'eye_exam':
        examType="eyeExam";
        break;
      case 'contact_exam':
        examType="lensExam";
      break;
    }
    this.router.navigate([`/${examType}/${this.patient_id}/${event.e_label}`]);

  }

  formChange(data){
    if(data.event.target.id == "e_type"){
      let group = data.group;
      if(group.value.e_type == "eye_exam"){
        this.config.accordions[0].rows[0][1].options=this.eye_exam_optios;
        this.group.controls['e_label'].setValue("STANDARD");
      }else if(group.value.e_type == "contact_exam"){
        this.config.accordions[0].rows[0][1].options=this.contact_exam_optios;
        this.group.controls['e_label'].setValue("FITTING");
      }
    }
  }
}
