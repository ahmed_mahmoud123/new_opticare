import { GenericPage } from './app.po';

describe('generic App', function() {
  let page: GenericPage;

  beforeEach(() => {
    page = new GenericPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
