$(document).ready(function () {
    $('[tabindex="1"]').focus();
    $('.example1').mask("99/99/9999",{placeholder: "__/__/____"});
    $('#size').mask("99-99-999",{placeholder: "00-00-000"});

    // Currency Format
    $('#cost,#selling_price').keyup(function(e){
      if(parseInt($(this).val())){
        $(this).val($(this).val().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    });

    //.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    $('select').on(
        'select2:select',(
            function(){
                $(this).focus();
            }
        )
    );


});
