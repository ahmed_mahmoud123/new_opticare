var express=require('express');
process.env.TZ = 'Africa/Nairobi';
var mongoose=require('mongoose');
var jwt = require("jsonwebtoken");
var patient=require('./server/routes/patient');
var fs=require("fs");
const APP_SECRET = "$@#$%$#^%%&%*&^(*&())";
const config=JSON.parse(fs.readFileSync(`${__dirname}/env.json`));
var app=express();
app.use(express.static('dist'));

app.use(function (req, res, next) {
    //res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'x-access-token,Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});
/*
app.use((req,resp,next)=>{
  let token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  if (token) {
    try {
        var decoded = jwt.decode(token, APP_SECRET);
        console.log(decoded);
        next();
      } catch (err) {
        resp.status(403).send("Not Authorized")
      }
  } else {
    resp.status(403).send("Not Authorized")
  }
});*/

mongoose.connect(config.development.DB_URL);

// Loading Routes
fs.readdirSync("./server/routes/").forEach(file=>{
    let route=require(`./server/routes/${file}`);
    app.use(`/${route.route_name}`,route);
});



app.get('/*',(req,resp)=>{
  resp.sendFile(__dirname+"/dist/index.html");
});



app.listen(process.env.PORT || 4000);
