var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let model=require('../models/refunds');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  model.add(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});


router.post('/get',jsonMiddelware,(req,resp)=>{
  model.get(req.body.date).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.post('/getRefundAmount',jsonMiddelware,(req,resp)=>{
  model.getRefundAmount(req.body.date).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});



router.route_name="refund";
module.exports=router;
