var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let inventoryModel=require('../models/inventory');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  inventoryModel.add_inventory(req.body).then(result=>{
    resp.json({ok:1});
  },err=>{
    resp.json(err);
  });
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  inventoryModel.update_inventory(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/get/:item_code',(req,resp)=>{
  inventoryModel.get_inventory(req.params.item_code).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/details/:id',(req,resp)=>{
  inventoryModel.get(req.params.id).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/max_item_code',(req,resp)=>{
  inventoryModel.get_max_item_code().exec(function(err,result){
    resp.json(result);
  });
});

router.get('/index/:page',(req,resp)=>{
  inventoryModel.search(undefined,req.params.page).then(inventories=>{
    resp.json(inventories);
  });
});


router.post('/search',jsonMiddelware,(req,resp)=>{
  inventoryModel.search(buildQuery(req.body.query),req.body.page).then(inventories=>{
    resp.json(inventories);
  },err=>console.log(err));
});


function buildQuery(queryObj){
  let query={};
  let tags=queryObj.tags;
  query["$and"]=[];
  if(Object.keys(queryObj).length == 0) return undefined;
  else if(queryObj.item_code){
    return queryObj;
  }
  if(queryObj.inventories && queryObj.inventories.length > 0)
    query["$and"].push({type:{'$in':queryObj.inventories}})

  if(tags && tags.length > 0)
  {
    let regex = tags.map(function (e) { return new RegExp(e, "i"); });
    let $orObject={"$or":[]};
    let fields=inventoryModel.get_fields();
    for (let i = 0; i < fields.length; i++) {
      let condition={};
      condition[fields[i]]={"$in": regex };
      $orObject["$or"].push(condition);
    }
    query["$and"].push($orObject);

  }
  if(queryObj.priceRange){
    console.log(queryObj.priceRange[0]);
    query["$and"].push({"selling_price":{"$gte":queryObj.priceRange[0],"$lte":queryObj.priceRange[1]}});
  }
  if(query["$and"].length == 0 ) return undefined;
  return query;
}

router.get('/delete/:ids',(req,resp)=>{
  inventoryModel.delete_inventory(req.params.ids).then(result=>{
    resp.json(result);
  });
});
router.route_name="inventory";
module.exports=router;
