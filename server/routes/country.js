var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let model=require('../models/country');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  model.add(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});


router.get('/get',(req,resp)=>{
  model.get().then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});


router.route_name="country";
module.exports=router;
