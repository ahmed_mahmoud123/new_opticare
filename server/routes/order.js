var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let orderModel=require('../models/order');
var mongoose=require('mongoose');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  orderModel.add_order(req.body).then(result=>{
    console.log("SUCCESS");
    resp.json(result.ops.pop());
  },err=>{
    console.log("ERROR");
    resp.json(err);
  });
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  orderModel.update_order(req.body).then(result=>{
    resp.json(req.body);
  },err=>{
    console.log(err);
    resp.json(err);
  });
});

router.post('/getTransactions',jsonMiddelware,(req,resp)=>{
  orderModel.getTransactions(req.body.date).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.post('/getRecipetDetails',jsonMiddelware,(req,resp)=>{
  let patientModel = mongoose.model('patients');
  orderModel.getRecipetDetails(req.body.receipt).then(result=>{
    patientModel.populate(result,{path:"patient_id",select:"first_name last_name"},(err,result)=>{
      resp.json(result);
    });
  },err=>{
    resp.json(err);
  });
});


router.get('/get/:order_id',(req,resp)=>{
  orderModel.get_order(req.params.order_id).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});


router.get('/max_order_number',(req,resp)=>{
  orderModel.get_max_order_number().exec(function(err,result){
    console.log(result);
    if(result.length)
      result = result.pop();
    resp.json(result);
  });
});



router.get('/delete/:ids',(req,resp)=>{
  orderModel.delete_order(req.params.ids).then(result=>{
    resp.json(result);
  });
});

router.post('/search',jsonMiddelware,(req,resp)=>{
  orderModel.search_order(buildQuery(req.body.query),req.body.page).then(patients=>{
    resp.json(patients);
  },err=>console.log(err));
});

router.post('/patient/:id',jsonMiddelware,(req,resp)=>{
  orderModel.search_order(buildQuery(req.body.query,req.params.id),req.body.page).then(patients=>{
    resp.json(patients);
  },err=>console.log(err));
});

function buildQuery(queryObj,p_id){
  let query={};
  console.log(p_id);
  let tags=queryObj.tags;
  query["$and"]=[];
  if(p_id){
    query["$and"].push({$or:[{patient_id:parseInt(p_id)},{patient_id:p_id}]})
  }
  if(Object.keys(queryObj).length == 0 && !p_id) return undefined;

  if(tags && tags.length > 0)
  {
    let regex = tags.map(function (e) { return new RegExp(e, "i"); });
    let $orObject={"$or":[]};
    let fields=orderModel.get_fields();
    for (let i = 0; i < fields.length; i++) {
      let condition={};
      condition[fields[i]]={"$in": regex };
      $orObject["$or"].push(condition);
    }
    query["$and"].push($orObject);
  }
  console.log(query);
  return query;
}

router.route_name="order";
module.exports=router;
