var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let settingModel = require('../models/setting');
let model=require('../models/cashes');
let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  model.add(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});


router.post('/get',jsonMiddelware,(req,resp)=>{
  model.get(req.body.date).then(result=>{
    if(!result){
      model.getOpeningCash().then(result=>{
        if(!result){
          model.getOpeningCashForSetting().then(result=>{
            resp.json(result);
          });
        }else{
          resp.json(result);
        }
      });
    }else{
      resp.json(result);
    }
  }).catch(err=>{
    console.log(err);
  });
});


router.route_name="cashes";
module.exports=router;
