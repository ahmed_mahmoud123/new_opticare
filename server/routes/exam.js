var express=require('express');
var bodyParser=require('body-parser');
let jsonMiddelware=bodyParser.json();
let examModel=require('../models/exam');

let router=express.Router();


router.post('/new',jsonMiddelware,(req,resp)=>{
  examModel.add_exam(req.body).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.post('/update',jsonMiddelware,(req,resp)=>{
  examModel.update_exam(req.body).then(result=>{
    console.log(result);
    resp.json(result);
  },err=>{
    console.log(err);
    resp.json(err);
  });
});

router.get('/get/:exam_id',(req,resp)=>{
  examModel.get_exam(req.params.exam_id).then(result=>{
    resp.json(result);
  },err=>{
    resp.json(err);
  });
});

router.get('/get_patient_exams/:patient_id',(req,resp)=>{
  examModel.get_patient_exams(req.params.patient_id).exec(function(err,result){
  
    resp.json(result);
  });
});

router.route_name="exam";
module.exports=router;
