var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Supplier{

  constructor(){
    this.init();
    this.model=mongoose.model('suppliers');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var supplier=new Schema({
      name:String
    });
    mongoose.model("suppliers",supplier);
  }
}

module.exports=new Supplier();
