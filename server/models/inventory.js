var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


class Inventory{

  get_fields(){
    return [
      "model",
      "model_number",
      "colour",
      "size",
      "quantity",
      "gender",
      "frame_type",
      "materials",
      "reorder_level",
      "cost",
      "spring_hinge",
      "selling_price",
      "lens_type",
      "lens_name",
      "material",
      "base_curve",
      "sph",
      "cyl",
      "axis",
      "near_add",
      "inter_add",
      "index",
      "model_name",
      "modality",
      "category",
      "notes",
    ];
  }
  constructor(){
    this.init();
    this.inventoryModel=mongoose.model('inventories');
  }



  add_inventory(inventoryData){
    let now=new Date();
    if(inventoryData.supplier == '-')
      inventoryData.supplier = null;
    if(inventoryData.brand == '-')
      inventoryData.brand = null;
    inventoryData.time=`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    inventoryData.datetime=+now;
    inventoryData.selling_price = Number(inventoryData.selling_price);
    return this.inventoryModel.collection.insert(inventoryData);
  }

  update_inventory(inventory){
    if(inventory.supplier == '-')
      inventory.supplier = null;
    if(inventory.brand == '-')
      inventory.brand = null;
    inventory.selling_price = Number(inventory.selling_price); 

    return this.inventoryModel.collection.update({item_code:inventory.item_code},{"$set":inventory});
  }

  get_inventory(item_code){
    return this.inventoryModel.find({item_code:item_code});
  }

  get(id){
    return this.inventoryModel.findOne({_id:id});
  }

  get_max_item_code(){
    return this.inventoryModel.findOne({},{item_code:true,_id:false}).sort({item_code:-1});
  }


  search(query=undefined,page=1){
    if(query)
      return this.inventoryModel.paginate(query,{populate:["supplier","brand","cl_brand","cl_name","lens_name"],sort:{_id:-1},page: page,limit: 10});
    else
      return this.inventoryModel.paginate({},{populate:["supplier","brand","cl_brand","cl_name","lens_name"],sort:{_id:-1},page: page,limit: 10});
  }

  delete_inventory(ids){
    ids=JSON.parse(ids);
    return this.inventoryModel.remove({_id:{"$in":ids}});
  }

  init(){
    var Schema=mongoose.Schema;
    var inventory=new Schema({
      supplier: {type: Schema.Types.ObjectId, ref: 'suppliers'},
      brand: {type: Schema.Types.ObjectId, ref: 'brands'},
      cl_brand: {type: Schema.Types.ObjectId, ref: 'cl_brands'},
      cl_name: {type: Schema.Types.ObjectId, ref: 'cl_names'},
      lens_name: {type: Schema.Types.ObjectId, ref: 'lens_names'},
      selling_price:{
        type:Number
      }
    });
    inventory.plugin(mongoosePaginate),
    mongoose.model("inventories",inventory);
  }
}

module.exports=new Inventory();
