var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Category{

  constructor(){
    this.init();
    this.model=mongoose.model('categories');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var category=new Schema({});
    mongoose.model("categories",category);
  }
}

module.exports=new Category();
