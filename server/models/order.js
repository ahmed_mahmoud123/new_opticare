var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


class Order{


  constructor(){
    this.init();
    this.orderModel=mongoose.model('orders');
  }

  get_fields(){
    return ["order_number","order_date","order_status","insurance_status","patient_name"]
  }


  getTransactions(date){
    return this.orderModel.aggregate([
      {$unwind:"$payment_summary"},
      {$match:{"payment_summary.p_date":date}},
      {$project:{payment_summary:true}}
    ]);
  }

  getRecipetDetails(receipt){
    console.log(Number(receipt));
    return this.orderModel.aggregate([
      {$unwind:"$payment_summary"},
      {$match:{"payment_summary.receipt":Number(receipt) } },
      {$project:{payment_summary:true,patient_id:true,order_number:true}}
    ]);
  }

  search_order(query=undefined,page=1){
    if(query)
      return this.orderModel.paginate(query,{ sort:{_id:-1},page: page,limit: 10});
    else
      return this.orderModel.paginate({},{sort:{_id:-1},page: page,limit: 10});
  }

  add_order(orderData){
    let now=new Date();
    let receipt = +now * Math.random();
    orderData.time=`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    orderData.datetime=+now;
    orderData.order_number = orderData.order_number.toString();
    if(orderData.payment_summary){
      if(orderData.payment1 != "-")
        orderData.payment_summary.push({balance_amount:orderData.balance_amount,receipt: receipt,p_date:orderData.order_date,p_method:orderData.payment1,comment:orderData.comment_method_1,p_amount:orderData.deposit_amount1})
      if(orderData.payment2 != "-")
        orderData.payment_summary.push({balance_amount:orderData.balance_amount,receipt: receipt,p_date:orderData.order_date,p_method:orderData.payment2,comment:orderData.comment_method_2,p_amount:orderData.deposit_amount2})
      orderData.amount_paid=0;
      for (let i = 0; i < orderData.payment_summary.length; i++) {
        if(orderData.payment_summary[i].p_method != "-")
          orderData.amount_paid+=parseInt(orderData.payment_summary[i].p_amount);
      }
    }
    return this.orderModel.collection.insert(orderData);
  }



  get_max_order_number(){
    return this.orderModel.find({},{order_number:true,_id:false}).sort({_id:-1}).limit(1);
  }


  update_order(order){
    let now=new Date();
    let receipt = +now * Math.random();
    let id = order._id;
    delete order['_id'];
    for (let property in order) {
      if(typeof order[property] === "string")
        order[property]=order[property].toUpperCase()
    }
    if(order.payment_summary){
      if(order.payment1 != "-")
        order.payment_summary.push({balance_amount:order.balance_amount, receipt: receipt, p_date:order.order_date,p_method:order.payment1,p_amount:order.deposit_amount1})
      if(order.payment2 != "-")
        order.payment_summary.push({balance_amount:order.balance_amount,receipt: receipt, p_date:order.order_date,p_method:order.payment2,p_amount:order.deposit_amount2})
      order.amount_paid=0;
      for (let i = 0; i < order.payment_summary.length; i++) {
        if(order.payment_summary[i].p_method != "-")
          order.amount_paid+=parseInt(order.payment_summary[i].p_amount);
      }
    }
    return this.orderModel.collection.update({order_number:id},{"$set":order});
  }

  get_order(order_id){

    return this.orderModel.findOne({order_number:order_id});
  }

  delete_order(ids){
    ids=JSON.parse(ids);
    return this.orderModel.remove({_id:{"$in":ids}});
  }

  init(){
    var Schema=mongoose.Schema;
    var order=new Schema({
      order_number:String
    });
    order.plugin(mongoosePaginate),
    mongoose.model("orders",order);
  }
}

module.exports=new Order();
