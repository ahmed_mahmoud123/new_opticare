var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Country{

  constructor(){
    this.init();
    this.model=mongoose.model('countries');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var country=new Schema({
      name:String
    });
    mongoose.model("countries",country);
  }
}
// Country routing ..
module.exports=new Country();
