var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class LensName{

  constructor(){
    this.init();
    this.model=mongoose.model('lens_names');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var lens_name=new Schema({});
    mongoose.model("lens_names",lens_name);
  }
}

module.exports=new LensName();
