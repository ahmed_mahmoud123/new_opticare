var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Brand{

  constructor(){
    this.init();
    this.model=mongoose.model('brands');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var brand=new Schema({});
    mongoose.model("brands",brand);
  }
}

module.exports=new Brand();
