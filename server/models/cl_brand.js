var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class CL_BRAND{

  constructor(){
    this.init();
    this.model=mongoose.model('cl_brands');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var brand=new Schema({});
    mongoose.model("cl_brands",brand);
  }
}

module.exports=new CL_BRAND();
