var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class District{

  constructor(){
    this.init();
    this.model=mongoose.model('districts');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var district=new Schema({});
    mongoose.model("districts",district);
  }
}

module.exports=new District();
