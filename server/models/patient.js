var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

class Patient{

  constructor(){
    this.init();
    this.patientModel=mongoose.model('patients');
  }

  get_fields(){
    return ["email","info","middle_name","first_name","last_name","dob","national_id","mobile","home_phone"]
  }

  add_patient(patientData){
    console.log(patientData);
    for (let property in patientData) {
      if(typeof patientData[property] === "string")
        patientData[property]=patientData[property].toUpperCase()
    }
    patientData["p_status"] = "active";
    let patient=new this.patientModel(patientData);
    return patient.save();
  }

  update_patient(patient){
    return this.patientModel.update({_id:patient._id},{"$set":patient});
  }

  delete_patient(ids){
    ids=JSON.parse(ids);
    return this.patientModel.remove({_id:{"$in":ids}});
  }

  patient_details(id){
    return this.patientModel.findOne({_id:id});
  }

  search_patient(query=undefined,page=1){
    console.log(JSON.stringify(query));
    if(query)
      return this.patientModel.paginate(query,{ sort:{_id:-1},page: page,limit: 10});
    else
      return this.patientModel.paginate({},{sort:{_id:-1},page: page,limit: 10});
  }

  all_data(){
    let self = this;
    return new Promise(function (resolve, reject) {
      self.patientModel.find({},{first_name:true,last_name:true},(err,result)=>{
        if(err) return resolve({error:err})
          let item;
          for (let i = 0; i < result.length; i++) {
            item=result[i];
            result[i] = {_id:item._id,name:item.first_name+" "+item.last_name};
          }
          return resolve(result)


      });
    })

  }

  init(){
    var Schema=mongoose.Schema;
    var CounterSchema =new Schema({
        _id: {type: String,default:"patientId"},
        p_id: {type: String},
        seq: { type: Number, default: 1 }
    });
    mongoose.model('counters', CounterSchema);
    var counter=mongoose.model('counters');
    new counter({}).save();
    var patient=new Schema({
      _id:Number,
      first_name:String,
      last_name:String,
      middle_name:String,
      national_id:String,
      title:String,
      gender:{
        type:String,
        enum:["MALE","FEMALE"]
      },
      dob:String,
      insured:String,
      insurance_company_name:String,
      insurance_number:String,
      country:String,
      state:String,
      city:String,
      district:String,
      address:String,
      postal_code:String,
      home_phone:String,
      work_phone:String,
      mobile:String,
      email:String,
      receive_methods:[Boolean],
      info:String,
      checked:{
        type:Boolean,
        default:false
      },
      p_status:String,
      p_status_arr:{
        type:[Boolean],
        default:[false,false,false]
      }
    });
    patient.plugin(mongoosePaginate);

    patient.pre('save', function(next) {
        var doc = this;
        counter.findOneAndUpdate({_id: 'patientId'}, {$inc: { seq: 1} }, function(error, counter)   {
          console.log(error);
            if(error)
                return next(error);
            if(counter)
              doc._id = counter.seq+"";
            else
              doc._id = "1";
            next();
        });
    });
    mongoose.model("patients",patient);
  }
}

module.exports=new Patient();
