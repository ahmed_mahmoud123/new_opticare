var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Cashes{

  constructor(){
    this.init();
    this.model=mongoose.model('cashes');
    this.settingsModel=mongoose.model('settings');
  }



  add(obj){
    console.log(obj);
    return this.model.findOneAndUpdate({date:obj.date},{"$set":obj}, {upsert:true});
    //return this.model.collection.insert(data);
  }



  update(id,obj){
    return this.model.update({},{"$set":obj});
  }

  get(date){
    return this.model.findOne({date:date});
  }

  getOpeningCash(){
    return this.model.findOne({}).sort({_id:-1}).limit(1)
  }

  getOpeningCashForSetting(){
    return this.settingsModel.findOne({});
  }

  init(){
    var Schema=mongoose.Schema;
    var cash=new Schema({
      date:String,
      sales:String,
      openingCash : String,
      petty_cash : String,
      tentativeClosingCash:String,
      bnk_deposite : String,
      discrepancy : String,
      refundAmount:String,
      actual_cache_in_hand:String
    });
    mongoose.model("cashes",cash);
  }
}

module.exports=new Cashes();
