var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class State{

  constructor(){
    this.init();
    this.model=mongoose.model('states');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var state=new Schema({
      name:String
    });
    mongoose.model("states",state);
  }
}

module.exports=new State();
