var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Setting{

  constructor(){
    this.init();
    this.model=mongoose.model('settings');
  }



  add(data){
    return this.model.collection.insert(data);
  }



  update(obj){
    return this.model.update({},{"$set":obj});
  }

  get(){
    return this.model.find({});
  }

  init(){
    var Schema=mongoose.Schema;
    var setting=new Schema({
      _id:Number,
      openingCash:Number
    });
    
    mongoose.model("settings",setting);
    mongoose.model("settings").findOneAndUpdate({
    },{_id:1,openingCash:5000} , {upsert:true}, function(error, result) {
      console.log(error);
    });
  }
}

module.exports=new Setting();
