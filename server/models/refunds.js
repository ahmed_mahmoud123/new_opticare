var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Refund{

  constructor(){
    this.init();
    this.model=mongoose.model('refunds');
  }



  add(data){
    return this.model.collection.insert(data);
  }

  get(date){
    return this.model.find({date:date});
  }

  getRefundAmount(date){
    return this.model.aggregate([
      {$match:{date:date} },
      {$group:{_id:"$date",sum:{"$sum":"$amount"}}},
      {$limit:1}
    ]);
  }
  init(){
    var Schema=mongoose.Schema;
    var refund=new Schema({

    });
    mongoose.model("refunds",refund);
  }
}

module.exports=new Refund();
