var mongoose=require('mongoose');

//var mongoosePaginate = require('mongoose-paginate');

class Exam{
  constructor(){
    this.init();
    this.examModel=mongoose.model('exams');
  }



  add_exam(examData){
    let now=new Date();
    for (let property in examData) {
      if(typeof examData[property] === "string")
        examData[property]=examData[property].toUpperCase()
    }
    examData.time=`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    examData.datetime=+now;
    this.hasPrismValue(examData);
    return this.examModel.collection.insert(examData);
    //return exam.save();
  }

  update_exam(exam){
    let id = exam._id;
    delete exam['_id'];
    for (let property in exam) {
      if(typeof exam[property] === "string")
        exam[property]=exam[property].toUpperCase()
    }
    this.hasPrismValue(exam);
    return this.examModel.collection.update({_id:mongoose.Types.ObjectId(id)},{"$set":exam});
  }
  get_exam(exam_id){
    return this.examModel.findOne({_id:exam_id});
  }

  get_patient_exams(patient_id){
    return this.examModel.find({patient_id:{$eq:parseInt(patient_id)}}).sort({_id:-1});
  }
  init(){
    var Schema=mongoose.Schema;
    var exam=new Schema({});
    //eyeExam.plugin(mongoosePaginate),
    mongoose.model("exams",exam);
  }

  hasPrismValue(exam){
    let hasPrismValue = false;
    let keys = Object.keys(exam);
    for (let j = 0; j < keys.length; j++) {
      if(keys[j].indexOf("prism")>=0 && (exam[keys[j]] != "-" && exam[keys[j]] != "")){
        console.log("hasPrismValue");
        hasPrismValue =true;
      }
    }
    exam.hasPrismValue = hasPrismValue;
  }
}

module.exports=new Exam();
